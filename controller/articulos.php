<?php
    if ($_GET)
    {
        $action = $_GET["action"];
        if (function_exists($action))
        {
            require("../model/articulos.php");
            call_user_func($action);
        }
    }

    function listar(){
        $articulos = new Articulos();
        $result = $articulos->getArticulos();
        
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }

    function listar2()
    {
        $articulos = new Articulos();
        $result = $articulos->getArticulos2();
        
        if (!$result)
        {
            die("no hay registros");
        }
        else
        {
            while($data = $result->fetch())
            {
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
            
        }
    }
    function guardar()
    {
        $codigo_articulo = $_POST["codigo_articulo"];
        $clave_articulo = $_POST["clave_articulo"];
        $descripcion_articulo = $_POST["descripcion_articulo"];
        $unidad = $_POST["unidad"];
        $codigo_barra = $_POST["codigo_barra"];
        $precio_c = $_POST["precio_c"];
        $precio_p = $_POST["precio_p"];
        $codigo_sat = $_POST["codigo_sat"];
        $descripcion_sat = $_POST["descripcion_sat"];
        $marca = $_POST["marca"];
        $cantidad = $_POST["cantidad"];
        $modelo = new Articulos();
        $result = $modelo->insertArticulo($codigo_articulo, $clave_articulo, $descripcion_articulo, $unidad, $codigo_barra, $precio_c, $precio_p, $codigo_sat, $descripcion_sat, $marca, $cantidad);
        echo $result;
    }


    function editar()
    {
        $id_articulo = $_POST["id_articulo"];
        $descripcion_articulo = $_POST["descripcion_articulo"];
        $unidad = $_POST["unidad"];
        $codigo_barra = $_POST["codigo_barra"];
        $clave_articulo = $_POST["clave_articulo"];
        $codigo_articulo1=$_POST["codigo_articulo"];
        $marca = $_POST["marca"];
        $precio_c = $_POST["precio_c"];
        $precio_p = $_POST["precio_p"];
        $codigo_sat = $_POST["codigo_sat"];
        $descripcion_sat = $_POST["descripcion_sat"];
        $cantidad = $_POST["cantidad"];      
        $articulo = new Articulos();
        $result = $articulo->updateArticulo($id_articulo, $descripcion_articulo, $unidad, $codigo_barra, $clave_articulo, $codigo_articulo1, $marca, $precio_c, $precio_p, $codigo_sat, $descripcion_sat, $cantidad);
        echo $result;
    }

    function eliminar()
    {
        $id_articulo = $_POST["id_articulo"];
        $articulo = new Articulos();
        $result = $articulo->deleteArticulo($id_articulo);
        echo $result;
    }

    function buscar()
    {
        $codigo_barra = $_POST["codigo_barra"];
        $articulo = new Articulos();
        $result = $articulo->lookArticulo($codigo_barra);
        echo $result;
    }
