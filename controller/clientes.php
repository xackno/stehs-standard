<?php
    if ($_GET)
    {
        $action = $_GET["action"];
        if (function_exists($action))
        {
            require("../model/clientes.php");
            call_user_func($action);
        }
    }

    function listar(){
        $clientes = new Clientes();
        $result = $clientes->getClientes();
        if (!$result){
            die("no hay registros");
        }else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }

function cliente(){
    require("../model/conexion_mysqli.php");

    $query="select *from clientes";
    $datosNombre=new stdClass();
    $result = $conexion->query($query);
    while($fila = $result->fetch_array()){
        $datosNombre->nombres[]= $fila['nombre_cliente'];
         $datosNombre->idC[]= $fila['id_cliente'];
         $datosNombre->colonia_cliente[]=$fila['colonia_cliente'];
         $datosNombre->credito[]= $fila['credito'];
    }   
    echo json_encode($datosNombre);
}

function CreditoCliente(){
    require("../model/conexion_mysqli.php");
    $id_cliente=$_POST['id_cliente'];
    $query="SELECT credito from clientes where id_cliente=$id_cliente";
    $result = $conexion->query($query);
      $CreditoCliente=new stdClass();

    while($fila = $result->fetch_array()){
        $CreditoCliente->credito= $fila['credito'];
    } 
    echo json_encode($CreditoCliente);
}

function EjecutarAbono(){
    require("../model/conexion_mysqli.php");
    $cantidad=$_POST['Cantidad_a_abonar'];
    $id_cliente=$_POST['id_cliente'];
    $tipo=$_POST['tipo'];
    $creditoActual=$_POST['creditoActual'];
    $usuario=$_POST['usuario'];
    $fechaHoy=$_POST['fechaHoy'];
    $id_turno=$_POST['id_turno'];

    $resp=new stdClass();
    $resp->te=$cantidad;
    
    $query="UPDATE clientes set credito=credito-$cantidad WHERE id_cliente=$id_cliente";
    $result=$conexion->query($query);

    $query2="INSERT INTO abonos VALUE(NULL,$id_cliente,NOW(),'$tipo',$cantidad,$creditoActual,$creditoActual-$cantidad,$id_turno)";
    $result2=$conexion->query($query2);
    //#buscando el nombre del cliente.#####################
    $query3="SELECT * from clientes where id_cliente='$id_cliente'";
    $result3=$conexion->query($query3);
    while($fila = $result3->fetch_array()){
        $nombreCliente= $fila['nombre_cliente'];
        }
        $resp->nombre=$nombreCliente;
    //%%%%%%%%%%%%%%%%OBTENER ID DEL CREDITO AGRGADO%%%%%%%%%%%5%%%%%%%%%%%%%
        $obtener_id_abono="SELECT MAX(id_abono) FROM abonos";
        $id_ab = $conexion->query($obtener_id_abono);
        $id_abono=0;
            while($fila2 = $id_ab->fetch_array()){
                $id_abono= $fila2['MAX(id_abono)'];
            }   
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //%%%%%%%%%%%%%%%%%%GUASRDANDO EN MOV_CLIENTE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    $insert_mov="INSERT INTO mov_cliente value(null,'$id_cliente','abono',$id_abono,now(),'$cantidad',($creditoActual-$cantidad))";
    $result_mov = $conexion->query($insert_mov);
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        


    echo json_encode($resp);

    //###################################################################################
    $info = file_get_contents("../view/js/imprimir.json");
    $proceso = json_decode($info, true);
    $proceso=$proceso["proceso"]+1;

$InfoTicket=array("proceso"=>$proceso,"tipo"=>"abono",'atendido'=>$usuario, 'fecha'=>$fechaHoy,'credito anterior'=>$creditoActual,'abono'=>$cantidad,'tipo de abono'=>$tipo,'saldo nuevo'=>$creditoActual-$cantidad,'cliente'=>$nombreCliente);

$json_venta=json_encode($InfoTicket);

$fp = fopen("../view/js/imprimir.json","w+");//(w+) replazar todo -- (a) agrega 
    fwrite($fp,  $json_venta."\n". PHP_EOL);
    fclose($fp);




    

}

function guardar(){
    $nombre_cliente = $_POST["nombre_cliente"];
    $domicilio_cliente = $_POST["domicilio_cliente"];
    $colonia_cliente=$_POST['colonia_cliente'];
    $municipio_cliente=$_POST['municipio_cliente'];
    $cp_cliente=$_POST['cp_cliente'];
    $telefono_cliente = $_POST["telefono_cliente"];
    $modelo = new Clientes();
    $result = $modelo->insertClientes($nombre_cliente, $domicilio_cliente, $colonia_cliente, $municipio_cliente, $cp_cliente, $telefono_cliente);
    echo $result;
}
function editar(){
    $id_cliente = $_POST["id_cliente"];
    $nombre_cliente = $_POST["nombre_cliente"];
    $domicilio_cliente = $_POST["domicilio_cliente"];
    $colonia_cliente=$_POST['colonia_cliente'];
    $municipio_cliente=$_POST['municipio_cliente'];
    $cp_cliente=$_POST['cp_cliente'];
    $telefono_cliente = $_POST["telefono_cliente"];
           
    $clientes = new Clientes();
    $result = $clientes->updateClientes($id_cliente, $nombre_cliente, $domicilio_cliente, $colonia_cliente, $municipio_cliente, $cp_cliente, $telefono_cliente);
    echo $result;
}
function eliminar(){
    $id_cliente = $_POST["id_cliente"];
    $cliente = new Clientes();
    $result = $cliente->deleteClientes($id_cliente);
    echo $result;
}

 