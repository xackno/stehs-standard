<?php
    if ($_GET)
    {
        $action = $_GET["action"];
        if (function_exists($action))
        {
            require("../model/marcas.php");
            call_user_func($action);
        }
    }

    function listar()
    {
        $marca = new Marcas();
        $result = $marca->getMarcas();
        
        if (!$result)
        {
            die("no hay registros");
        }
        else
        {
            while($data = $result->fetch())
            {
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
            
        }
    }

    function guardar()
    {
        $nombre_marca = $_POST["nombre_marca"];
        $modelo = new Marcas();
        $result = $modelo->insertMarcas($nombre_marca);
        echo $result;
    }


    function editar()
    {
        $id_marca = $_POST["id_marca"];
        $nombre_marca = $_POST["nombre_marca"];     
        $marca = new Marcas();
        $result = $marca->updateMarcas($id_marca, $nombre_marca);
        echo $result;
    }

    function eliminar()
    {
        $id_marca = $_POST["id_marca"];
        $marca = new Marcas();
        $result = $marca->deleteMarcas($id_marca);
        echo $result;
    }