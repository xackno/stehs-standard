<?php
    if ($_GET)
    {
        $action = $_GET["action"];
        if (function_exists($action))
        {
            require("../model/proveedor.php");
            call_user_func($action);
        }
    }


    function listar_pro_vend(){
        $proveedor = new Proveedor();
        $result = $proveedor->listar_pro_vend();
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }


    function listar_historial_movimiento(){
        $proveedor = new Proveedor();
        $result = $proveedor->listarhistorial();
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }
    function listarsalidas(){
        $proveedor = new Proveedor();
        $result = $proveedor->listarsalidas();
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }
    
    function listar(){
        $proveedor = new Proveedor();
        $result = $proveedor->getProveedores();
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }

    
    function listar_turno(){
        $proveedor = new Proveedor();
        $result = $proveedor->listar_turno();
        if (!$result){
            die("no hay registros");
        }
        else{
            while($data = $result->fetch()){
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
        }
    }

    function listar2()
    {   
        //$id_proveedor = $_POST["id_proveedor"];
        $alumno = new Alumno();
        //$result = $alumno->getAlumno2($id_proveedor);
        $result = $alumno->getAlumno2();
        if (!$result)
        {
            die("no hay registros");
        }
        else
        {
            while($data = $result->fetch())
            {
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
            
        }
    }

    function guardar()
    {
        $nombre_proveedor = $_POST["nombre_proveedor"];
        $telefono_proveedor = $_POST["telefono_proveedor"];
        $direccion = $_POST["direccion"];
        $modelo = new Proveedor();
        $result = $modelo->insertProveedor($nombre_proveedor, $telefono_proveedor, $direccion);
        echo $result;
    }


    function editar()
    {
        $id_proveedor = $_POST["id_proveedor"];
        $nombre_proveedor = $_POST["nombre_proveedor"];
        $telefono_proveedor = $_POST["telefono_proveedor"];
        $direccion = $_POST["direccion"];       
        $proveedor = new Proveedor();
        $result = $proveedor->updateProveedor($id_proveedor, $nombre_proveedor, $telefono_proveedor, $direccion);
        echo $result;
    }

    function eliminar()
    {
        $id_proveedor = $_POST["id_proveedor"];
        $proveedor = new Proveedor();
        $result = $proveedor->deleteProveedor($id_proveedor);
        echo $result;
    }