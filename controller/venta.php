<?php
    if ($_GET)
    {
        $action = $_GET["action"];
        if (function_exists($action))
        {
            require("../model/venta.php");
            call_user_func($action);
        }
    }

    function listar()
    {
        $venta = new Ventas();
        $result = $venta->getVentas();
        
        if (!$result)
        {
            die("no hay registros");
        }
        else
        {
            while($data = $result->fetch())
            {
                $datos["data"][] =$data;
            }
            echo json_encode($datos);
            
        }
    }

   