<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="view/img/logo.jpg" type="image/x-icon">
    <title>Stehspro V20</title>
    <link rel="stylesheet" href="view/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/vendor/iconos/css/font-awesome.min.css">
    <link rel="stylesheet" href="view/vendor/admin/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="view/vendor/iconos/css/iconos.min.css">
    <link rel="stylesheet" href="view/vendor/admin/css/estilos.css">
  <link rel="stylesheet" type="text/css" href="view/vendor/admin/css/jquery-ui-1.10.4.custom.min.css">
  <link rel="stylesheet" type="text/css" href="view/vendor/admin/css/jquery.autocomplete.css">
  <link rel="stylesheet" type="text/css" href="view/js/style.css">   

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>

<body>
<?php
    session_start();
    
    if(!isset($_SESSION["usuario"])){
        header("location:login.php");
    }
?>
<style type="text/css">
    .logo{display: inline-block;}
    ul li a i{color:white;font-size: 25px}
</style>
    <div id="wrapper" class="toggled">
        <div id="sidebar-wrapper">
            <div class="sidebar">
                <div style="width: 100%;text-align: center"><a href="http://www.stehs.com/">
                    <img src="view/img/logo.jpg" style="width: 50%;" >
                </a>
                    
                    <h5 id="system-Name"></h5>
                </div>
                
                <div class="avatar">
                    <span id="span_usuario_log"></span>
                    <input type="text" name="" id="tipo_usuario_log" value="" class="d-none">

                </div>
                <ul class="sidebar-menu">
                    
                    <li id="ventasXack"><a href="./?view=ventas" ><i class="zmdi zmdi-money"></i>Ventas</a></li>
                    <li id="articulos"><a href="./?view=articulos"><i class="zmdi zmdi-storage"></i>Productos</a></li>
                    <li id="marcas"><a href="./?view=marcas"><i class="zmdi zmdi-map"></i>Departamentos Marcas</a></li>
                    <li id="reporte"><a href="./?view=reporte"><i class="fa fa-clipboard"></i>Reporte</a></li>
                    <!-- <li id="pedidos"><a href="./?view=pedidos"><i class="fa fa-clipboard"></i>pedidos</a></li> -->
                    <hr style="border:1px solid white">
                    <li id="clientes"><a href="./?view=clientes"><i class="fa fa-user"></i>Clientes | Sucursal </a></li>
                    <!-- <li id="usuariosApp"><a href="./?view=usuariosApp"><i class="fa fa-mobile-phone text-primary"></i>Usuarios APP MOVIL</a></li> -->
                    
                    <li id="movimiento_inventario"><a href="./?view=movimiento_inventario"><i class="fa fa-clipboard"></i>Historial movimiento inventario</a></li>

                    <li id="configuracion"><a href="./?view=configuracion"><i class="fa fa-gear"></i>Configuración</a></li>

                    
                   
                </ul><br><br><br><br><br>
                <p >&copy <span id="copy"></span>, 2020.</p>
            </div>
    </div>
        <!-- Contenido -->
        <div id="page-content-wrapper" >
            <nav class="navbar bg-navbar" style="background-color: #262F39">
                <label style="color: white" id="usuario_log" class="d-none"><?php echo $_SESSION["usuario"];  ?></label>
                <a href="#menu-toggle" class="btn btn-sm btn-menu btn-warning" id="menu-toggle"><i class="zmdi zmdi-more-vert"></i></a>
                <label></label>

            
                <div class="dropdown show"   style="margin-left: 70%">
                  <a class="btn  btn-sm " style="color:white" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    <i class="fa fa-bell" ></i><sup style="padding:2px;background: red;color: white;border-radius: 5px" id="i_alert">0 </sup>
                  </a>
                  <?php
                    require("model/conexion_mysqli.php");
                    $query2="SELECT *FROM server_info";
                    $result = $conexion->query($query2);
                    if($result->num_rows > 0){
                        while($fila = $result->fetch_array()){
                            $id= $fila['id_servidor'];
                            $nombre= $fila['nombre'];
                            $url= $fila['url'];
                            $url_master=$fila['url_master'];
                        }   
                    }//fin if
                  ?>
                  <input type="number" id="id_server" class="d-none" value="<?php echo  $id;?>">

                  <div class="dropdown-menu" id="lista_alertas" aria-labelledby="dropdownMenuLink" style="padding: 5px">
                    
                  </div>
                </div>






<!--window modal ######modal alertas################-->
  <div class="modal fullscreen-modal fade" id="modalAlerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="fa fa-bell" style="color:#FF336F"></i>Alerta</h3>
        </div>
        <div class="modal-body">
            <div id="div_para_aviso"></div>
            <input type="number" id="id_alerta_actual"  class="d-none">
            <h6 id="fecha_alerta"></h6>
            <hr>
            <h5 style="text-align: justify;" id="contenido_alerta">
               
            </h5>
            <hr>
        </div>
        <hr>
        <button class="btn btn-success" id="Cambiar-status-alerta"><i class="fa fa-eye"></i> Visto</button>

      </div>
    </div>
  </div>



                <a href="logout.php" class="btn btn-info rounded-circle border-0" style="float: right;"><i class="fa fa-power-off"></i></a>
            </nav>
            <div class="page-content">
            <?php
            $url="";
            $get=$_GET["view"];
                if(isset($get) && $get!=""){
                    $url = "view/".$get.".php";
                    //echo $get;
                    if(file_exists($url)){
                        include $url;  
                    }
                }if ($get=="") {
                    $url = "view/ventas.php";
                    // echo "<h1 class='text-primary'>Página no encontrada, Favor de cerrar Sesión y volver a logearse...</h1>";
                }

                ?> 
                </div>

            
            </div>
             
        </div>
            <?php 
                if ( $get=="ventas") {
                    include "view/js/footer.php";
                }
                
         ?>
</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="view/vendor/jquery/jquery.min.js"></script>
    <script src="view/vendor/popper/popper.min.js"></script>
    <script src="view/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="view/vendor/admin/js/jquery.dataTables.min.js"></script>
    <script src="view/vendor/admin/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="view/vendor/admin/js/jquery-ui.js"></script>
    <script src="view/vendor/assets/js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script type="text/javascript" src="view/js/appVentas.js"></script>
    <script type="text/javascript" src="view/js/admin.js"></script>
    <script type="text/javascript" src="view/js/pedidos.js"></script>
        <script  type="text/javascript" src="view/vendor/admin/js/listar.js"></script>


</html>


<script type="text/javascript">
    
// $("#i_alert").fadeOut("slow",'swing' );
setInterval( function(){
    $("#i_alert").fadeOut("slow",'swing' );
    $("#i_alert").fadeIn("slow",'swing' )
},3000);


//$$$$$$$$$$$$$$$$$LISTANDO ALERTAS EN DROPDOWN$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// listandoalertas();
function listandoalertas(){
    $.ajax({
        xhrFields: {
            withCredentials: false
    },
    crossDomain: true,
    url:"<?php print($url_master);?>model/admin.php?action=listarAlertas",
    type:"post",
    dataType:"json",
    data:{
        id_server:$("#id_server").val()
    }
    }).done(function(r){
        // alert(r.id.length);
         $("#lista_alertas").html('');
        $("#i_alert").html(r.id.length);
        for(var x=0;x<r.id.length;x++){
            var texto="";
            if (r.text[x].length>=15) {
                for(var y=0;y<15;y++){
                    texto=texto+(r.text[x][y]);
                }
                texto=texto+"...";
            }else{
                texto=r.text[x];
            }
            
            $("#lista_alertas").append('<div class="alert alert-'+r.tipo[x]+' mis_alertas" role="alert" id="'+r.id[x]+'">'+texto+'</div>');
        }
    });
}
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $("#lista_alertas").on('click','div',function(){
        $("#fecha_alerta").html('');
        var id=$(this).attr("id");
        // alert(id);
        $.ajax({
            xhrFields: {
            withCredentials: false
            },
            crossDomain: true,
            url:"<?php echo $url_master;?>model/admin.php?action=getAlerta",
            type:"post",
            dataType:"json",
            data:{id:id}
        }).done(function(r){
            // alert(r.text);
            $("#modalAlerta").modal();
            $("#fecha_alerta").html(r.created);
            $("#contenido_alerta").html(r.text);
            $("#id_alerta_actual").val(r.id);
        });
    });//fin onclick/
    //#######################################CAMBIAR STATUS/#############################
    $("#Cambiar-status-alerta").click(function(){
        $.ajax({
            xhrFields: {
            withCredentials: false
            },
            crossDomain: true,
            url:"<?php echo $url_master;?>model/admin.php?action=changeStatusAlert",
            type:"post",
            dataType:"json",
            data:{
                id:$("#id_alerta_actual").val(),
            }
        }).done(function(r){
            if (r.status=="success") {
                $("#div_para_aviso").html("<div class='alert alert-success' role='alert'>listo</div>");
                setTimeout(function(){
                $( "#div_para_aviso").html('');
                }, 3500);
                $("#modalAlerta").modal("hide");
                listandoalertas();
            }else{
                $("#div_para_aviso").html("<div class='alert alert-success' role='alert'>Error. contacte con su administrador.</div>");
                setTimeout(function(){
                $( "#div_para_aviso").html('');
                }, 3500);
            }
        });
    });



</script>