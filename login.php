<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Punto Venta</title>
    <link rel="stylesheet" href="view/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/vendor/iconos/css/font-awesome.min.css">
    <link rel="stylesheet" href="view/vendor/iconos/css/iconos.min.css">
	<link rel="stylesheet" href="view/vendor/admin/css/estilos.css">
</head>
<body> 
    <nav class="navbar navbar-dark bg-dark">
      <font color="white"><h5>SISTEMA StehsPro v-7.0</h5></font>

       <a href="#Modal" data-toggle="modal" class="navbar-brand"><i class="fa fa-user-circle fa-lg"></i></a>
        <div class="modal fade" id="Modal" aria-hidden="true" >
            <div class="modal-dialog" >

                <div class="modal-content" style="margin-top: 150px;border-radius: 80px 100% 50px 100% ;">
                    <div class="modal-header" style="background: #78281F;color:white;border-radius: 50px 50px 0px 0px;width: 69%">
                        <h6 class="modal-title"> <img src="view/img/logo.jpg" style="width: 8%"> ENTRAR</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="comprueba_login.php" method="post">
                            <div class="form-group" style="margin-left: 10%">
                                <label for="login" class="form-control-label">USUARIO:</label>
                                <div class="input-group mb-2 mb-sm-0" style="width: 70%">
                                    <div class="input-group-addon"><i class="fa fa-user-circle" style="color:blue"></i></div>
                                    <input type="text" name="login" class="form-control" id="login" autofocus="autofocus" >
                                </div>
                            </div>
                            <div class="form-group" style="margin-left: 20%">
                                <label for="password" class="form-control-label">CONTRASEÑA:</label>
                                <div class="input-group mb-2 mb-sm-0" style="width: 80%">
                                    <div class="input-group-addon"><i class="fa fa-lock fa-lg" style="color: green"></i></div>
                                    <input type="password" name="password" class="form-control" id="password">
                                 </div>
                            </div>
                            <div class="modal-footer" style="margin-left: 30%">
                                <button type="submit" name="enviar" id="iniciar" class="btn btn-primary"><i class="fa fa-arrow-circle-right"></i></button>
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>

<style type="text/css">
    #login,#password{
        color: black;
        font-family:cursive;
    }
    #login:hover,#password:hover{
        border:3px solid #78281F;
    }
</style>

   
   <div class="container-fluid">
    <div class="row slider">
        <div class="col">
             <div class="carousel slide" id="slider" data-ride="carousel" >
                <ol class="carousel-indicators">
                    <li data-target="#slider" data-slide-to="0" class="active"></li>
                    <li data-target="#slider" data-slide-to="1"></li>
                    <li data-target="#slider" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="view/img/background1.jpg" alt="1" id="cambio_img" style="width: 100%;height: auto" class="d-block img-fluid">
                        <div class="carousel-caption d-none d-md-block">
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
    
    
    <script src="view/vendor/jquery/jquery.min.js"></script>
	<script src="view/vendor/popper/popper.min.js"></script>
	<script src="view/vendor/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

<script type="text/javascript">
    $("#Modal").modal("show");
    var cont=2;
    setInterval(function(){ 
        
        $('#cambio_img').attr('src', 'view/img/background'+cont+'.jpg');
        if (cont==8) {
            cont=1;
        }
        cont++;
     }, 30000);
</script>