<?php

    require("conexion.php");

    class Articulos
    {
        public function getArticulos()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select id_articulo,codigo_articulo,clave_articulo,descripcion_articulo,unidad,codigo_barra,precio_c,precio_p,nombre_marca,cantidad from articulos inner join marcas on articulos.marca=marcas.id_marca");
            return $consulta;
        }

        public function getArticulos2()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select codigo_articulo,codigo_barra from articulos");
            return $consulta;
        }
        // falta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // public function getAlumno2()
        public function getArticulo2($id_articulo)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from articulos where id_articulo = :id_articulo");
            // $consulta = $conexion->query('select * from alumnos inner join tutores on alumnos.id_tutor=tutores.id_tutor inner join padres on alumnos.id_padre=padres.id_padre where id_proveedor = :id_proveedor');
           $consulta->bindParam(':id_articulo', $id_articulo, PDO::PARAM_INT);
           $consulta->execute();
           return $consulta->fetch();
           // return $consulta;
        }
       
        //falta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
        public function getArticulo($id_articulo)
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from articulos inner join marcas on articulos.marca=marcas.id_marca where id_articulo = :id_articulo ");
            $consulta->bindParam(":id_articulo", $id_articulo, PDO::PARAM_INT);
            $consulta->execute();
            return $consulta->fetch();
        } 
        
        public function insertArticulo($codigo_articulo, $clave_articulo, $descripcion_articulo, $unidad, $codigo_barra, $precio_c, $precio_p, $codigo_sat, $descripcion_sat, $marca, $cantidad)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("insert into articulos value(null,:codigo_articulo, :clave_articulo, :descripcion_articulo, :unidad, :codigo_barra, :precio_c, :precio_p, :codigo_sat, :descripcion_sat, :marca, :cantidad)");
            $consulta->bindParam(":codigo_articulo", $codigo_articulo, PDO::PARAM_INT);
            $consulta->bindParam(":clave_articulo", $clave_articulo, PDO::PARAM_STR);
            $consulta->bindParam(":descripcion_articulo", $descripcion_articulo, PDO::PARAM_STR);
            $consulta->bindParam(":unidad", $unidad, PDO::PARAM_STR);
            $consulta->bindParam(":codigo_barra", $codigo_barra, PDO::PARAM_STR);
            $consulta->bindParam(":precio_c", $precio_c, PDO::PARAM_STR);
            $consulta->bindParam(":precio_p", $precio_p, PDO::PARAM_STR);
            $consulta->bindParam(":codigo_sat", $codigo_sat, PDO::PARAM_STR);
            $consulta->bindParam(":descripcion_sat", $descripcion_sat, PDO::PARAM_STR);
            $consulta->bindParam(":marca", $marca, PDO::PARAM_INT);
            $consulta->bindParam(":cantidad", $cantidad, PDO::PARAM_STR);
            return $consulta->execute();   
        }
        
        public function updateArticulo($id_articulo, $descripcion_articulo, $unidad, $codigo_barra, $clave_articulo, $codigo_articulo, $marca, $precio_c, $precio_p, $codigo_sat, $descripcion_sat, $cantidad)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("update articulos set clave_articulo = :clave_articulo, descripcion_articulo = :descripcion_articulo, unidad = :unidad, codigo_barra = :codigo_barra, codigo_articulo=:codigo, precio_c = :precio_c, precio_p = :precio_p, codigo_sat = :codigo_sat,descripcion_sat = :descripcion_sat, marca = :marca, cantidad = :cantidad where id_articulo = :id_articulo");
            $consulta->bindParam(":id_articulo", $id_articulo, PDO::PARAM_INT);
            $consulta->bindParam(":descripcion_articulo", $descripcion_articulo, PDO::PARAM_STR);
            $consulta->bindParam(":unidad", $unidad, PDO::PARAM_STR);
            $consulta->bindParam(":codigo_barra", $codigo_barra, PDO::PARAM_STR);
            $consulta->bindParam(":codigo", $codigo_articulo, PDO::PARAM_STR);
            $consulta->bindParam(":clave_articulo", $clave_articulo, PDO::PARAM_STR);
            $consulta->bindParam(":marca", $marca, PDO::PARAM_INT);
            $consulta->bindParam(":precio_c", $precio_c, PDO::PARAM_STR);
            $consulta->bindParam(":precio_p", $precio_p, PDO::PARAM_STR);
            
            $consulta->bindParam(":codigo_sat", $codigo_sat, PDO::PARAM_STR);
            
            $consulta->bindParam(":descripcion_sat", $descripcion_sat, PDO::PARAM_STR);
            $consulta->bindParam(":cantidad", $cantidad, PDO::PARAM_STR);
            return $consulta->execute();
        }
        
        public function deleteArticulo($id_articulo)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("delete from articulos where id_articulo = :id_articulo");
            $consulta->bindParam(":id_articulo", $id_articulo, PDO::PARAM_INT);
            return $consulta->execute();
        }

        public function lookArticulo($codigo_barra)
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("SELECT * FROM `articulos` WHERE `codigo_barra` =  :codigo_barra");
            $consulta->bindParam(":codigo_barra", $codigo_barra, PDO::PARAM_STR);
            $consulta->execute();
            return $consulta->fetch();
        }

    }

