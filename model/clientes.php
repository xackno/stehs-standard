<?php

    require("conexion.php");

    class Clientes 
    {
        public function getClientes()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from clientes");
            return $consulta;
        }       
        public function getCliente($id_cliente)
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from clientes where id_cliente = :id_cliente");
            $consulta->bindParam(":id_cliente", $id_cliente, PDO::PARAM_INT);
            $consulta->execute();
            return $consulta->fetch();
        }
        
        public function insertClientes($nombre_cliente, $domicilio_cliente, $colonia_cliente, $municipio_cliente, $cp_cliente, $telefono_cliente)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("insert into clientes value(null, :nombre_cliente,:domicilio_cliente,:colonia_cliente,:municipio_cliente, :cp_cliente, :telefono_cliente,0.00)");
            $consulta->bindParam(":nombre_cliente", $nombre_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":domicilio_cliente", $domicilio_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":colonia_cliente", $colonia_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":municipio_cliente", $municipio_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":cp_cliente", $cp_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":telefono_cliente", $telefono_cliente, PDO::PARAM_STR);
            
            return $consulta->execute();   
        }
        
        public function updateClientes($id_cliente, $nombre_cliente, $domicilio_cliente, $colonia_cliente, $municipio_cliente, $cp_cliente, $telefono_cliente){
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("update clientes set nombre_cliente = :nombre_cliente, telefono_cliente = :telefono_cliente, domicilio_cliente = :domicilio_cliente, colonia_cliente=:colonia_cliente, municipio_cliente=:municipio_cliente, cp_cliente=:cp_cliente where id_cliente = :id_cliente");
            $consulta->bindParam(":id_cliente", $id_cliente, PDO::PARAM_INT);
            $consulta->bindParam(":nombre_cliente", $nombre_cliente, PDO::PARAM_STR);
             $consulta->bindParam(":domicilio_cliente", $domicilio_cliente, PDO::PARAM_STR);

            $consulta->bindParam(":colonia_cliente", $colonia_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":municipio_cliente", $municipio_cliente, PDO::PARAM_STR);
            $consulta->bindParam(":cp_cliente", $cp_cliente, PDO::PARAM_STR);

            $consulta->bindParam(":telefono_cliente", $telefono_cliente, PDO::PARAM_STR);
           
            return $consulta->execute();
        }
        
        public function deleteClientes($id_cliente)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("delete from clientes where id_cliente = :id_cliente");
            $consulta->bindParam(":id_cliente", $id_cliente, PDO::PARAM_INT);
            return $consulta->execute();
        }



    }//fin class

