<?php

    require("conexion.php");

    
    class consultas_general
    {
      
        public function getProveedores()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from proveedores");
            return $consulta;   
        }

        public function getMarcas()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from marcas");
            return $consulta;   
        }

        public function getprecios()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from articulos");
            return $consulta;   
        }

        public function getArticulos()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from articulos");
            return $consulta;   
        }
        
    }
