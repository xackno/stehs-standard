<?php

    require("conexion.php");

    class Marcas 
    {
        public function getMarcas()
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from marcas");
            return $consulta;
        }

        public function getMarca($id_marca)
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from marcas where id_marca = :id_marca");
            $consulta->bindParam(":id_marca", $id_marca, PDO::PARAM_INT);
            $consulta->execute();
            return $consulta->fetch();
        }
        
        public function insertMarcas($nombre_marca)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("insert into marcas value(null, :nombre_marca)");
            $consulta->bindParam(":nombre_marca", $nombre_marca, PDO::PARAM_STR);
            return $consulta->execute();   
        }
        
        public function updateMarcas($id_marca, $nombre_marca)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("update marcas set nombre_marca = :nombre_marca where id_marca = :id_marca");
            $consulta->bindParam(":id_marca", $id_marca, PDO::PARAM_INT);
            $consulta->bindParam(":nombre_marca", $nombre_marca, PDO::PARAM_STR);
            return $consulta->execute();
        }
        
        public function deleteMarcas($id_marca)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("delete from marcas where id_marca = :id_marca");
            $consulta->bindParam(":id_marca", $id_marca, PDO::PARAM_INT);
            return $consulta->execute();
        }
    }
