<?php

    require("conexion.php");

class Proveedor {



        public function listar_pro_vend(){
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("SELECT *FROM productos_vendidos INNER JOIN articulos ON productos_vendidos.producto_vendido=articulos.id_articulo");
            return $consulta;
        }

        public function listar_turno(){
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("SELECT *FROM turnos");
            return $consulta;
        }
        public function listarhistorial(){
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("SELECT *FROM historial_movimiento");     
            return $consulta;
        }

        public function listarsalidas(){
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("SELECT *FROM salidas INNER JOIN turnos ON salidas.id_turno=turnos.id_turno");     
            return $consulta;
        }









        public function getProveedores(){
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->query("select * from proveedores");
            return $consulta;
        }

        // falta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // public function getAlumno2()
        public function getAlumno2($id_proveedor)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from alumnos inner join tutores on alumnos.tutor_id=tutores.id_tutor inner join padres on alumnos.padre_id=padres.id_padre where id_proveedor = :id_proveedor");
            // $consulta = $conexion->query('select * from alumnos inner join tutores on alumnos.id_tutor=tutores.id_tutor inner join padres on alumnos.id_padre=padres.id_padre where id_proveedor = :id_proveedor');
           $consulta->bindParam(':id_proveedor', $id_proveedor, PDO::PARAM_INT);
           $consulta->execute();
           return $consulta->fetch();
           // return $consulta;
        }
       
        //falta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        public function getProveedor($id_proveedor)
        {
            $modelo = new Conexion;
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("select * from proveedores where id_proveedor = :id_proveedor");
            $consulta->bindParam(":id_proveedor", $id_proveedor, PDO::PARAM_INT);
            $consulta->execute();
            return $consulta->fetch();
        }
        
        public function insertProveedor($nombre_proveedor, $telefono_proveedor,$direccion)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("insert into proveedores value(null, :nombre_proveedor, :telefono_proveedor, :direccion)");
            $consulta->bindParam(":nombre_proveedor", $nombre_proveedor, PDO::PARAM_STR);
            $consulta->bindParam(":telefono_proveedor", $telefono_proveedor, PDO::PARAM_INT);
            $consulta->bindParam(":direccion", $direccion, PDO::PARAM_STR);
            return $consulta->execute();   
        }
        
        public function updateProveedor($id_proveedor, $nombre_proveedor, $telefono_proveedor, $direccion)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("update proveedores set nombre_proveedor = :nombre_proveedor, telefono_proveedor = :telefono_proveedor, direccion = :direccion where id_proveedor = :id_proveedor");
            $consulta->bindParam(":id_proveedor", $id_proveedor, PDO::PARAM_INT);
            $consulta->bindParam(":nombre_proveedor", $nombre_proveedor, PDO::PARAM_STR);
            $consulta->bindParam(":telefono_proveedor", $telefono_proveedor, PDO::PARAM_STR);
            $consulta->bindParam(":direccion", $direccion, PDO::PARAM_STR);
            return $consulta->execute();
        }
        
        public function deleteProveedor($id_proveedor)
        {
            $modelo = new Conexion();
            $conexion = $modelo->conectar();
            $consulta = $conexion->prepare("delete from proveedores where id_proveedor = :id_proveedor");
            $consulta->bindParam(":id_proveedor", $id_proveedor, PDO::PARAM_INT);
            return $consulta->execute();
        }
    }

    //$alumnos = Alumno::deleteAlumno(3); se elimina un alumno por id

   // $alumnos = Alumno::updateAlumno(1, "prueba", "prueba", "prueba", "prueba", "prueba", 1, "a", "prueba", "prueba");   //sirvera para actualizar un alumno por id
    
   // $alumnos = Alumno::insertAlumno("prueba", "prueba", "prueba", "prueba", "prueba", 1, "a", "prueba", "prueba");  //asi se insertan datos

   // $alumnos = Alumno::getAlumno(2);   //sirve para consultar los alumnos agregados o el id de un alumno
   // foreach ($alumnos as $alumno)
   // {
       // print_r($alumno);
   // }