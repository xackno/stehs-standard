<div class="page-content-header">
    <div class="btn-group pull-right">
        <button type="button" href="#Modal" class="btn btn-sm btn-dark" data-toggle="modal" >Nuevo</button>
         <button id="mostrarModalExistencia" class="btn btn-info">Existencia</button>
    </div>
    <h3>
    <i class="zmdi zmdi-store" style="color: orange;"></i>Articulos<i class="zmdi zmdi-mall" style="color: purple;"></i>
    </h3>
</div>



<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modalExistencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-shopping-cart-plus" style="color:#FF336F"></i>Actualizar existencia</h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col">
                    <div class="input-group">
                        <input type="text"  id="findProductoExistencia" style="width: 50%;float: left;color:blue;"  class="form-control" placeholder="Buscar producto" autocomplete="off"> 

                        <button class="btn btn-primary" id="btn_buscarExistencia" style="padding: 2px"><i class="zmdi zmdi-search" style="color: white;font-size: 215%;padding: 0px;"></i></button>
                    </div>
                </div>
                <div class="col">
                    <label><b>Tipo de actualización</b></label>
                    <input type="checkbox" data-onstyle="info" data-offstyle="danger" id="tipo_actualizacion" data-toggle="toggle" data-style="slow" data-on="ajuste" data-off="nuevos" data-width="100px">
                </div>
            </div>
          <div class="table_responsive">
            <div style="height: 320px;overflow-x: scroll;">
               <table id="tabla_busquedaExistencia" class="table table-bordered display" >
               <thead>
                   <tr>
                    <th style="font-size: 10px">Nueva entrada</th>
                    <th style="font-size: 10px" class="d-none">Anterior</th>
                    <th>Id</th>
                    <th style="width: 100%">Descripción</th>  
                   </tr>
               </thead>
               <tbody id="tb_busquedaExistencia" >
        
               </tbody>
           </table>   
            </div>
           

            <br><br>
            <button class="btn btn-primary" id="guardar_existencia">Guardar cambio</button> 
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- fin modal busqueda -->







<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="box box-blue">
                <div class="box-body">
                    <div class="table_responsive">
                      <table id="tab_articulos5" class="table table-bordered display" width="100%">
                           <thead>
                               <tr>
                                    <th width="20%">Clave</th>
                                    <th width="60%">Descripción</th>
                                    <th>Unidad</th>
                                    <th>Cód. Barras<p></p><i class="fa fa-barcode fa-lg"></i></th>
                                    <th>A la Compra</th>
                                    <th>A la Venta</th>
                                    <th>Marca</th>
                                    <th width="-20%">Cant</th>
                                    <th width="20%"><i class="zmdi zmdi-settings zmdi-lg"></i></th>

                               </tr>
                           </thead>
                       </table>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model content from here -->
<style type="text/css">
    .horiz{display: inline-block;}
    #tab_articulos thead,tbody tr th,td{padding: 0px;margin:0;}
</style>
       

<div class="modal fade" id="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title text-white" style="text-align: center;">Registro de Articulos</h4>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <!-- <form action="comprueba_login.php" method="post"> -->
                <form id="form" method="post" onsubmit="return articulo();">
                    <div class='row'>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="form-group">
                        <label for="codigo_articulo" class="form-control-label">Codigo del producto:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">#</div>
                            <input type="text" name="codigo_articulo" class="form-control" id="codigo_articulo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="clave_articulo" class="form-control-label">Clave del producto:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-alert-circle"></i></div>
                            <input type="text" name="clave_articulo" class="form-control" id="clave_articulo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion_articulo" class="form-control-label">Descripción:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-wrap-text"></i></div>
                            <input type="text" name="descripcion_articulo" class="form-control" id="descripcion_articulo">
                         </div>
                    </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="form-group">
                        <label for="unidad" class="form-control-label">Unidad:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-confirmation-number"></i></div>
                            
                            <select name="unidad" class="form-control" id="unidad">
                                <option value="PZA">Pieza</option>
                                <option value="BLS">Bolsa</option>
                                <option value="Caja">Caja</option>
                                <option value="Exhib">Exhibidor</option>
                                <option value="Arp">Arpilla</option>
                            <option value="Bulto">Bulto</option>
                            <option value="LT.">Litro</option>  
                                <option value="PAQ">Paquete</option>
                                <option value="KG.">Kilogramo</option>
                                <option value="GR">Gramo</option>
                                <option value="PAR">Par</option>
                                <option value="M. ">Metros</option>
                                <option value="M3.">Metros <sup>3</sup></option>
                                

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="precio_c" class="form-control-label">Precio a la Compra:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-money-off">C</i></div>
                            <input type="text" name="precio_c" class="form-control" id="precio_c">
                         </div>
                    </div>
                    <div class="form-group">
                        <label for="precio_p" class="form-control-label">Precio a la Venta:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-money-off">L</i></div>
                            <input type="text" name="precio_p" class="form-control" id="precio_p">
                         </div>
                    </div>
                    <div class="form-group">
                        <label for="codigo_sat" class="form-control-label">Codigo del SAT:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">#</div>
                            <input type="text" name="codigo_sat" class="form-control" id="codigo_sat">
                        </div>
                    </div>
                    
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            
                    <div class="form-group">
                        <label for="descripcion_sat" class="form-control-label">Descripción Sat:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-file-text"></i></div>
                            <input type="text" name="descripcion_sat" class="form-control" id="descripcion_sat">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="marca" class="form-control-label">Marca:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="zmdi zmdi-comment-image"></i></div>
                           <select  class="form-control" name="marca" id="marca">
                                    <option selected="" disabled="">--- Selecciona una opción ---</option>
                                    <?php
                                    require "model/consultas_general.php";
                                    $modelo = new consultas_general();
                                    $modelo = $modelo->getMarcas();
                                    foreach ($modelo as $I)
                                    {
                                    ?>
                                    <option value="<?php echo $I['id_marca']; ?>"><?php echo $I["nombre_marca"]; ?></option>
                                    <?php
                                    }
                                    ?>  
                                </select>
                       </div> 
                    </div>
                    <div class="form-group">
                        <label for="cantidad" class="form-control-label">Cantidad en existencia:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">C #</div>
                            <input type="text" name="cantidad" class="form-control" id="cantidad">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="codigo_barra" class="form-control-label">Codigo de barra:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="fa fa-barcode"></i></div>
                            <input type="text" name="codigo_barra" class="form-control" id="codigo_barra">
                         </div>
                    </div>
                        </div>
                    </div>
                <!-- <form action="controller/proveedor.php?action=guardar" method="post"> -->
                    
                    <div class="modal-footer">
                        <button type="submit" name="enviar" class="btn btn-primary">GUARDAR</button>
                        <button type="reset" class="btn btn-warning">BORRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!--window modal ######modal autenticación################-->
<div class="modal fade" id="ModalAutenticación" aria-hidden="true">
  <div class="modal-dialog"  role="document">
      <div class="modal-content">
          <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-card" style="color:#FF336F"></i> Autenticación</h3>
        </div>
          <div class="modal-body">
            <input type="password" id="PassCliente" autocomplete="off" placeholder="Ingrese la contraseña del administrador"  class="form-control">
            <input type="text" class="d-none" id="NamUsuario" value="<?php echo$_SESSION["usuario"]?>">
            <br>
            <button class="btn btn-danger" style="float: right;" id="ejecutar">Aceptar</button>
            <p id="status" class="text-secondary"></p>
          </div>
      </div>
  </div>
</div> 



