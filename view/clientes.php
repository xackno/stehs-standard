

<div class="page-content-header">
    <div class="btn-group pull-right">
        <button type="reset" href="#Modal" class="btn btn-sm btn-dark" data-toggle="modal" >Nuevo</button>
        <button type="reset" href="#ModalRecuperacion" id="RecuperacionCredito" class="btn btn-success" data-toggle="modal" >Recuperación de Crédito</button>
        <p id="turno" class="d-none"></p>
    </div>
    <h3>
        <i class="zmdi zmdi-account" style="color: green;"></i></i>
        Clientes
        <!-- <small>Listados</small> -->
    </h3>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="box box-blue">
                <div class="box-body">
                    <div class="table_responsive">
                      <table id="tab_clientes" class="table table-bordered display">
                           <thead>
                               <tr>
                                    <th>Nombre</th>
                                    <th>Domicilio</th>
                                    <th>Colonia</th>
                                    <th>Municipio</th>
                                    <th>CP</th>
                                    <th>Telefono</th>
                                    <th>Crédito</th>
                                    <th>Acciones</th>
                               </tr>
                           </thead>
                       </table>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model content from here -->
       

<div class="modal fade" id="Modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style=" align-items: center;">
            <div class="modal-header">
                <h6 class="modal-title" style="text-align: center;">Registro de clientes</h6>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <!-- <form action="comprueba_login.php" method="post"> -->
                <form id="form1" method="post" onsubmit="return clientes();">        
      <div class="form-group">
          <label for="nombre_cliente" class="form-control-label">Nombre:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
              <input type="text" name="nombre_cliente" class="form-control" id="nombre_cliente" autocomplete="off">
          </div>
      </div>
       <div class="form-group">
          <label for="domicilio_cliente" class="form-control-label">Domicilio:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
              <input type="text" name="domicilio_cliente" class="form-control" id="domicilio_cliente" autocomplete="off">
           </div>
      </div> 
      <div class="form-group">
          <label for="colonia_cliente" class="form-control-label">Colonia o comunidad:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
              <input type="text" name="colonia_cliente" class="form-control" id="colonia_cliente" autocomplete="off">
           </div>
        </div>
      <div class="form-group">
          <label for="municipio_cliente" class="form-control-label">Municipio y distrito:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
              <input type="text" name="municipio_cliente" class="form-control" id="municipio_cliente"autocomplete="off">
           </div>
      </div>
      <div class="form-group">
          <label for="cp_cliente" class="form-control-label">Código postal:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
              <input type="number" name="cp_cliente" class="form-control" id="cp_cliente" autocomplete="off">
           </div>
      </div>
      <div class="form-group">
          <label for="telefono_cliente" class="form-control-label">Telefono:</label>
          <div class="input-group mb-2 mb-sm-0">
              <div class="input-group-addon"><i class="fa fa-phone fa-lg"></i></div>
              <input type="number" name="telefono_cliente" class="form-control" id="telefono_cliente" autocomplete="off">
           </div>
      </div>
                   
      <div class="modal-footer">
          <button type="submit" name="enviar" class="btn btn-primary" >GUARDAR</button>
          <button type="reset" class="btn btn-warning">BORRAR</button>
      </div>
    </form>
            </div>
        </div>
    </div>
</div>



<!--window modal ######modal RECUPERACIÓN################-->
<div class="modal fade" id="ModalRecuperacion" aria-hidden="true">
  <div class="modal-dialog"  role="document">
      <div class="modal-content">
          <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-card" style="color:#FF336F"></i> Recuperación de crédito</h3>
        </div>
          <div class="modal-body">
             <select name="clientes" class="form-control btn-primary" id="clientesPrestamo" >
              <option disabled="true" selected="true">Selecciones un cliente</option>   
             </select>

             <label for="credito">Crédito actual:</label>
             <input type="number" name="credito" id="creditoActual" disabled="true" class="form-control">
             <label for="abonar">Abonar:</label>
             <input type="number" name="abonar" id="Cantidad_a_abonar"  class="form-control">
             <label for="tipo">Tipo:</label>
             <select name ="tipo" id="tipo" class="form-control">
               <option value="Efectivo">Efectivo</option>
               <option value="Tranferencia">Tranferencia</option>
               <option value="Deposito">Depósito</option>
               <option value="Cheque">Cheque</option>
             </select>
             <br><br>
             <button id="btn_abonar" class="btn btn-danger">Abonar</button>
             <p id="usuario" class="d-none"> <?php echo $_SESSION["usuario"] ?></p> 
             <p id="fecha" class="d-none"></p>
             <p id="hora" class="d-none"></p>
          </div>
      </div>
  </div>
</div>    
<!--window modal ######modal autenticación################-->
<div class="modal fade" id="ModalAutenticación" aria-hidden="true">
  <div class="modal-dialog"  role="document">
      <div class="modal-content">
          <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-card" style="color:#FF336F"></i> Autenticación</h3>
        </div>
          <div class="modal-body">
            <input type="password" id="PassCliente" autocomplete="off" placeholder="Ingrese la contraseña del administrador"  class="form-control">
            <input type="text" class="d-none" id="NamUsuario" value="<?php echo$_SESSION["usuario"]?>">
            <button class="btn btn-danger" id="ejecutar">Aceptar</button>
            <p id="status" class="text-secondary"></p>
          </div>
      </div>
  </div>
</div> 









<!--window modal ######modal HISTORIAL################-->
  <div class="modal fullscreen-modal fade" id="modalHistorial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-account" style="color:#FF336F"></i> Historial del cliente: <span id="idCliente"></span> </h3>
        </div>
      <div class="modal-content">
        <div class="modal-body">
          <div id="seccionImprimir">
              <label id="nombre">Nombre:</label>
            <label id="domicilio" style="float: right;">Domicilio:</label>
            <br>
            <label id="telefono">Telefono:</label>
            <label id="credito" style="float: right;color: red;">Credito actual:</label>

          <div class="table_responsive">
            <table id="tabla_historial" style="font-size: 70%" class="table table-bordered display" >
                 <thead>
                     <tr>
                        <th >Tipo</th>
                        <th># Ticket</th>
                        <th>Fecha</th>
                        <th>F.Limite</th>
                        <th>Cant</th>
                        <th>Productos</th>
                        <th>Precio</th>
                        <th>Importe</th>
                        <th>Pago inicial</th>
                        <th>Saldo</th>
                     </tr>
                 </thead>
                 <tbody>
          
                 </tbody>
             </table>   
          </div>
          </div>
          <button class="btn btn-success"  id="crearPDF">Imprimir PDF</button>
        </div>
      </div>
    </div>
  </div>
<!-- fin modal busqueda -->

