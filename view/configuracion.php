
<br>
<div id="alertas" style=""></div>
<br><br>
<div class="card" style="width: 80%;margin-left: 10%">
  <ul class="list-group list-group-flush" >
  	<li class="list-group-item " style="background:#193737;color: white"><i class="fa fa-cog"></i> Configuración</li>
    <li class="list-group-item">Cambiar nombre del sistema
     <input type="checkbox" data-onstyle="success" id="cambiar_nombre" data-toggle="toggle" data-style="slow" data-on="Si" data-off="No">
     <div id="li1">
     	<label>Nuevo nombre</label>
     	<input type="text" name="" id="text_newname" class="form-control" placeholder="ingrese nuevo nombre" style="width:40% "><br>
     	<button class="btn btn-primary" id="btn_cambiar_nombre">Cambiar</button>
     </div>
</li>
    <li class="list-group-item" style="width: 100%">Cambiar Logotipo<span style="width: 100%"></span> 
    	<input type="checkbox" data-onstyle="success" id="cambiarlogo" data-toggle="toggle" data-style="slow" data-on="Si" data-off="No">

		<form id="subirImg" name="subirImg" enctype="multipart/form-data" method="post" action="">
			<br>
			<span class="text-secondary">Selecciones una imagen con extensión .png no mayor a 400 kb.</span><br>
		  <label for="imagen">Subir imagen:</label>
		  <input type="hidden" name="MAX_FILE_SIZE" value="2000000" class="btn btn-primary" />
		  <input type="file" name="imagen" id="imagen" />
		  <input type="submit" name="subirBtn" id="subirBtn" value="Subir imagen" class="btn btn-info" />
		</form>

</li>
 <li class="list-group-item" style="width: 100%">Agregar Usuarios <span style="width: 100%"> 
  <button class="btn btn-primary" id="agregarUsuario">Agregar</button></span>
  
  <br><br>
      <div id="div_agregar" class="columna" >
          <label>Usuario</label>
          <input type="text" name="" class="form-control" id="user">
          <label>Contraseña</label>
          <input type="password" name="" class="form-control" id="pass">
          <label>Tipo</label>
          <select class="form-control" id="type">
            <option selected="true" disabled="true">Seleccione </option>
              <option value="admin">Administrador</option>
              <option value="operador">Operador</option>
              <option value="vendedor">Vendedor</option>
          </select>
          <br>
          <button class="btn btn-success" style="float: right;" id="inputUser">Agregar</button>

      </div>

      
</li>
<style type="text/css">
  .columna{width: 45%;display: inline-block;}
</style>
<li class="list-group-item"><p>Deshabilitar actualización de existencia</p>
     <input type="checkbox" data-onstyle="success" id="cambiar_existencia" style="width: 30px;height: 50px"  >
      <div id="li3">
      <label>Nuevo</label>
      <input type="text" name="" class="form-control" placeholder="ingrese nuevo" style="width:40% "><br>
      <button class="btn btn-primary">Cambiar</button>
     </div>
</li>
<li class="list-group-item">Ver Usuarios <button class="btn btn-info"  id="verUsers">ver</button>
     <div  id="listaUsers" >
        <h5>Usuarios del sistema</h5>  
        <div class="table-responsive">
          <table class="table table-bordered"  id="tb_usuario" width="100%" cellspacing="0">
            <thead class="table" style="background: #3D9F5A ">
              <tr>
                <th>id</th>
                <th>usuario</th>
                <th>contraseña</th>
                <th>tipo</th>
                <th>estado</th>
                <th><i class="fa fa-gear"></i>
                  
                </th>
              </tr>
            </thead>
            <tbody >


            </tbody>
          </table>
        </div>
        <br><br><br><br><br>
      </div>
      
</li>
    <!-- <li class="list-group-item">Cambiar copyright
     <input type="checkbox" data-onstyle="success" id="cambiar_copy" data-toggle="toggle" data-style="slow" data-on="Si" data-off="No">
    	<div id="li3">
     	<label>Nuevo</label>
     	<input type="text" name="" class="form-control" placeholder="ingrese nuevo" style="width:40% "><br>
     	<button class="btn btn-primary">Cambiar</button>
     </div>
</li> -->
  </ul>
</div>


<!--window modal ######modal editar user################-->
  <div class="modal fullscreen-modal fade" id="editar_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3> Editar</h3>
        </div>
        <div class="modal-body">
          <div id="alertas5">
            
          </div>
          <input type="number" name="" id="id_user" class="d-none"><br>
          <label>Usuario</label>
          <input type="text" name="" id="usuario" class="form-control">
          <label>Contraseña</label>
          <input type="password" name="" id="password" class="form-control" disabled="true">
          <label>Tipo</label>
          <select class="form-control" id="tipo">
            <option value="admin">Administrador</option>
            <option value="vendedor">Vendedor</option>
            <option value="operador">Operador</option>
          </select>
          <br><br>
          <label>Habilitar</label>
          <input type="checkbox"  id="status"  >
          <br><br>
          <button class="btn btn-sm btn-success" id="guardarEdicion" style="float: right;">Guardar</button>
        </div>
      </div>
    </div>
  </div>




<?php
if (isset($_POST['subirBtn'])) {
if ($_FILES['imagen']['type'] == "image/png") {
if ($_FILES['imagen']['size'] <= 20000000) {
copy($_FILES['imagen']['tmp_name'], "view/img/logo.jpg");
}
}
}
?>