<div class="card">
  <div class="card-header" style="background: #A569BD;color:white">
    
    <div class="row">
    	<div class="col">
    		<h4>ESTADO FINANCIERO</h4>
    	</div>
    	<div class="col"></div>
    	<div class="col"></div>
    	<div class="col">
    		<button class="btn btn-success"><i class="fa fa-plus"></i> Gasto Operación</button>
    	</div>
    </div>
  </div>
  <div class="card-body" >
  	<p>Seleccione el periodo de fecha a mostrar:</p>
  	<div class="row">
  		<div class="col">
  			<div class="input-group">
		  		<input type="date" name="date1" class="form-control">
		  		<h4> A </h4>
		  		<input type="date" name="date2" class="form-control">
		  		<button class="btn btn-primary">Ver</button>
		  	</div>
  		</div>
  		<div class="col"></div>
  		<div class="col"></div>
  	</div>
  	<br>
  	<div style="border:1px solid #afafaf;width: 50%;height:600px;margin: auto;text-align: center">
  		<h4>ESTADOS DE RESULTADOS PARA <b>STEHS</b></h4>
  		<h6 class="text-info">FECHA: DEL 01-DE-JULIO AL 31 DE AGOSTO 2020</h6>
  		<br>
  		<hr class="bg-success" style="width: 80%">
  		<div class="row" style="margin-left:10px;margin-right: 10px">
  			<div class="col text-left" >
  				<p>VENTAS AL PUBLICO:</p>
  				<p>COSTO DE VENTAS (precio a la compra)</p>
  			</div>
  			<div class="col"></div>
  			<div class="col text-right">
  				<p><b>$100 000.00</b></p>
  				<p><b>$30 000.00</b></p>
  			</div>
  		</div>
  	</div>
  	

  </div>
</div>