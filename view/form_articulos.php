<?php
if (isset($_GET["accion"]) && $_GET["accion"]=="editar"):
    require("model/articulos.php");
    require("model/conexion_mysqli.php");
    $articulo = @Articulos::getArticulo($_GET["id_articulo"]);
?>
    <div class="page-content-header">
        <h5>
            <i class="zmdi zmdi-book"></i>
        Articulo
        <small>Actualizar Datos</small>
        </h5>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="box box-blue">
                    <div class="box-body">
                        <form id="form5" method="post">
                            <h1 class="text-secondary">Datos del Articulo</h1>
                            <hr>
                            <div class="form-group">
                                <input type="text" class="form-control d-none" id="id_articulo" name="id_articulo" value="<?php echo $articulo["id_articulo"]; ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control d-none" id="descripcion_sat" name="descripcion_sat" value="<?php echo $articulo["descripcion_sat"]; ?>">
                            </div>
                            <div class="form-group">
                                <h5 for="codigo_articulo">Codigo de articulo:</h5>
                                    <input type="text" class="form-control"  autocomplete="off"id="codigo_articulo" name="codigo_articulo" value="<?php echo $articulo["codigo_articulo"]; ?>">
                            </div>
                            <div class="form-group">
                                <h5 for="clave_articulo" >Clave de articulo:</h5>
                                    <input type="text" class="form-control" autocomplete="off" id="clave_articulo" name="clave_articulo" value="<?php echo $articulo["clave_articulo"]; ?>">
                            </div>
                            <div class="form-group">
                                
                                <h5 for="marca" >marca:</h5>
                                
                                <select id="marca" name="marca" class="form-control" >
                                <option value="<?php echo $articulo["marca"]; ?>" class="text text-success"><?php echo $articulo["nombre_marca"]; ?></option>
                                <?php 
                                $consulta="SELECT *FROM marcas";
                                $resutado=$conexion->query($consulta);      
                                while($fila = $resutado->fetch_array()){
                                ?>
                                    <option value="<?php echo $fila['id_marca'];?>"><?php echo $fila['nombre_marca']; ?> </option>
                                <?php
                             }
                                ?>
                                 </select>

                            </div>
                            <div class="form-group">
                                <h5 for="descripcion_articulo">Descripción:</h5>
                                    <input type="text" class="form-control"  autocomplete="off" id="descripcion_articulo" name="descripcion_articulo" value="<?php echo $articulo["descripcion_articulo"]; ?>">
                            </div>
                            <div class="form-group">
                                <h5 for="unidad">Unidad:</h5>
                                    <input type="text" autocomplete="off" class="form-control" id="unidad" name="unidad" value="<?php echo $articulo["unidad"]; ?>">
                            </div>
                            <div class="form-group">
                                <h5 for="codigo_barra">Código de barra:</h5>
                                    <input type="text" autocomplete="off" class="form-control" id="codigo_barra" name="codigo_barra" value="<?php echo $articulo["codigo_barra"]; ?>">
                            </div>
                            

                            <div class="form-group">
                                <h5 for="precio_c">Precio a la compra :</h5>
                                    <input type="text" autocomplete="off" class="form-control" id="precio_c" name="precio_c" value="<?php echo $articulo["precio_c"]; ?>">
                            </div>

                            <div class="form-group">
                                <h5 for="precio_p" >Precio a la Venta:</h5>
                                    <input type="text" autocomplete="off" class="form-control" id="precio_p" name="precio_p" value="<?php echo $articulo["precio_p"]; ?>">
                            </div>
                            
                            <div class="form-group">
                                <h5 for="codigo_sat">Codigo SAT:</h5>
                                    <input type="text"  autocomplete="off" class="form-control" id="codigo_sat" name="codigo_sat" value="<?php echo $articulo["codigo_sat"]; ?>">
                            </div>
                            <div class="form-group">
                                <h5 for="cantidad">Cantidad en existencia:</h5>
                                    <input type="text"  readonly="true" autocomplete="off" class="form-control" id="cantidad" name="cantidad" value="<?php echo $articulo["cantidad"]; ?>">
                            </div>
                            <hr>
                            <div class="text-center">
                                <button id="editarticulo" class="btn btn-primary btn-sm"><i class="fa fa-folder-o fa-lg"></i> Actualizar</button>
                                <a href="./?view=articulos" class="btn btn-sm btn-success"><i class="zmdi zmdi-mall"></i> Articulos</a>
                            </div>
                         </form>
                    </div>
                </div>
            </div>
         </div>
    </div>

<!-- //////////////////////////////////////////////////////////// -->

<?php
elseif (isset($_GET["accion"]) && $_GET["accion"]=="listar"):
    require("model/articulos.php");
    $articulo = @Articulos::getArticulo2($_GET["id_articulo"]);
?>
<div class="page-content-header">
    <div class="btn-group pull-right">
    </div>
    <h5>
        <i class="zmdi zmdi-face"></i>
        Información del SAT
        <!-- <small>Datos</small> -->
    </h5>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="box box-blue">
                <div class="box-body">
                    <div class="table_responsive">
                      <table class="table table-bordered text-center">
                        <thead>
                            <tr>
                            <td><b>Codigo SAT</b></td>
                            <td><b>Descripción Sat</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
    // foreach ($articulos as $articulo)
                        {
                        ?>
                        <tr>
                            <td><?php echo $articulo['codigo_sat']; ?></td>
                            <td><?php echo $articulo['descripcion_sat']; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                       </table>      
                    </div>
                    <a href="./?view=articulos" class="btn btn-sm btn-success"><i class="zmdi zmdi-assignment-return"></i> Articulos</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
endif;
?>