<?php
    require("model/clientes.php");
    $cliente = @Clientes::getCliente($_GET["id_cliente"]);
?>
    <div class="page-content-header">
        <h3>
            <i class="zmdi zmdi-book"></i>
        Clientes
        <small>Actualizar Datos</small>
        </h3>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="box box-blue">
                    <div class="box-body">
                        <form id="form" method="post">
                            <p class="text-secondary">Datos del Cliente</p>
                            <hr>
                            <div class="form-group">
                                <input type="text" class="form-control d-none" id="id_cliente" name="id_cliente" value="<?php echo $cliente["id_cliente"]; ?>">
                            </div>
                            <div class="form-group">
                                <label for="nombre_cliente" class="col-sm-2 col-form-label text-center">Nombres:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" value="<?php echo $cliente["nombre_cliente"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="domicilio_cliente" class="col-sm-2 col-form-label text-center">Domicilio:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="domicilio_cliente" name="domicilio_cliente" value="<?php echo $cliente["domicilio_cliente"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="colonia_cliente" class="col-sm-2 col-form-label text-center">Colonia:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="colonia_cliente" name="colonia_cliente" value="<?php echo $cliente["colonia_cliente"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="municipio_cliente" class="col-sm-2 col-form-label text-center">Municipio:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="municipio_cliente" name="municipio_cliente" value="<?php echo $cliente["municipio_cliente"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cp_cliente" class="col-sm-2 col-form-label text-center">Código postal:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cp_cliente" name="cp_cliente" value="<?php echo $cliente["cp_cliente"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telefono_cliente" class="col-sm-2 col-form-label text-center">Teléfono:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="telefono_cliente" name="telefono_cliente" value="<?php echo $cliente["telefono_cliente"]; ?>">
                                </div>
                            </div>
                            
                             
                            <hr>
                            <div class="text-center">
                                <button id="editCliente" class="btn btn-primary btn-sm"><i class="fa fa-folder-o fa-lg"></i> Actualizar</button>
                                <a href="./?view=clientes" class="btn btn-sm btn-success"><i class="zmdi zmdi-account"></i> clientes</a>
                                
                            </div>
                         </form>
                    </div>
                </div>
            </div>
         </div>
    </div>

<!-- //////////////////////////////////////////////////////////// -->
