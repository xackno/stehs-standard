<?php
    require("model/marcas.php");
    $marca = @Marcas::getMarca($_GET["id_marca"]);
?>
    <div class="page-content-header">
        <h3>
            <i class="zmdi zmdi-book"></i>
        Clientes
        <small>Actualizar Datos</small>
        </h3>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="box box-blue">
                    <div class="box-body">
                        <form id="form" method="post">
                            <p class="text-secondary">Datos del Cliente</p>
                            <hr>
                            <div class="form-group">
                                <input type="text" class="form-control d-none" id="id_marca" name="id_marca" value="<?php echo $marca["id_marca"]; ?>">
                            </div>
                            <div class="form-group">
                                <label for="nombre_marca" class="col-sm-2 col-form-label text-center">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre_marca" name="nombre_marca" value="<?php echo $marca["nombre_marca"]; ?>">
                                </div>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button id="editMarca" class="btn btn-primary btn-sm"><i class="fa fa-folder-o fa-lg"></i> Actualizar</button>
                                <a href="./?view=marcas" class="btn btn-sm btn-success"><i class="zmdi zmdi-account"></i> Marcas</a>
                                
                            </div>
                         </form>
                    </div>
                </div>
            </div>
         </div>
    </div>

<!-- //////////////////////////////////////////////////////////// -->
