<?php
    require("model/proveedor.php");
    $proveedor = @Proveedor::getProveedor($_GET["id_proveedor"]);
?>
    <div class="page-content-header">
        <h3>
            <i class="zmdi zmdi-book"></i>
        Proveedores
        <small>Actualizar Datos</small>
        </h3>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="box box-blue">
                    <div class="box-body">
                        <form id="form" method="post">
                            <p class="text-secondary">Datos del Proveedor</p>
                            <hr>
                            <div class="form-group">
                                <input type="text" class="form-control d-none" id="id_proveedor" name="id_proveedor" value="<?php echo $proveedor["id_proveedor"]; ?>">
                            </div>
                            <div class="form-group">
                                <label for="nombre_proveedor" class="col-sm-2 col-form-label text-center">Nombres:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor" value="<?php echo $proveedor["nombre_proveedor"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telefono_proveedor" class="col-sm-2 col-form-label text-center">Telefono:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="telefono_proveedor" name="telefono_proveedor" value="<?php echo $proveedor["telefono_proveedor"]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="col-sm-2 col-form-label text-center">direccion:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $proveedor["direccion"]; ?>">
                                </div>
                            </div>
                             </div>
                            <hr>
                            <div class="text-center">
                                <button id="editproveedor" class="btn btn-primary btn-sm"><i class="fa fa-folder-o fa-lg"></i> Actualizar</button>
                                <a href="./?view=proveedores" class="btn btn-sm btn-success"><i class="zmdi zmdi-account"></i> Proveedor</a>
                            </div>
                            
                         </form>
                    </div>
                </div>
            </div>
         </div>
    </div>

<!-- //////////////////////////////////////////////////////////// -->
