
$(document).ready(function(){

$("#tb_usuariosApp tbody").html('');
cargarpedidos();
listarUserApp();







//--------------------------------------------------------
  $("#tb_usuariosApp tbody").on('click','tr',function(){
    $("#modal_User").modal('show');
    var id=$(this).find('td:first-child').html();
    var nombre=$(this).find('td:nth-child(2)').html();
    var estatus=$(this).find('td:nth-child(9)').find("span").html();
    $("#idUserApp").html(id);
    $("#nombreUsuario").html(nombre);
    if (estatus=='true') {
        $('#activar').attr("checked","true");
    }else{
       $('#activar').removeAttr("checked");
    }

  });
//---------------------------------------------
  $("#activarCuenta").click(function(){
    var activar="";
      if( $('#activar').prop('checked') ) {
        activar="true";
        }else{
          activar="false";
        }
      $.ajax({
        url:'view/js/admin.php?action=activarusuario',
        type:'post',
        dataType:'json',
        data:{
          id:$("#idUserApp").html(),
          activar:activar
        }
      }).done(function(e){
        $("#tb_usuariosApp tbody").html('');
        if (e.status=="true") {
          $("#alertasactivar").html('<div class="alert alert-success" role="alert">Operación correcta</div>');
              setTimeout(function(){
                $("#alertasactivar").html('');
              }, 5000);
         }else{
         $("#alertasactivar").html('<div class="alert alert-danger" role="alert">Error de operación. consulte con su administrador</div>');
              setTimeout(function(){
                $("#alertasactivar").html('');
              }, 5000);
         }
        listarUserApp();
      });
  });


//############################FOR PEDIDO#####################################
//######################cambiar de status de los pedidos##################
  $("#DivPedidos_imprimir").hide();
  $("#printPedidos").hide();
  $("#comentarios").hide();



  $("#cambiarstatus").click(function(){
  	var status=$("#nuevoStatus").val();
  	if (status!=null) {
  	var comentario=$("#comentario").val();
  	$.ajax({
        url:'view/js/admin.php?action=cambiarstatus',
        type:'post',
        dataType:'json',
        data:{
        	id:$("#id_pedido").html(),
        	status:status,
        	comentario:comentario
        }
      }).done(function(r){
      			// alert(r.status);
      		 if (r.status=="true") {
      		 	$("#alertasstatus").html('<div class="alert alert-success" role="alert">Cambiado correctamente.</div>');
                setTimeout(function(){
                  $("#alertasstatus").html('');
                }, 5000);
      		 }else{
      		 $("#alertasstatus").html('<div class="alert alert-danger" role="alert">Error al cambiar, verifique la información o consulte al administrador.</div>');
                setTimeout(function(){
                  $("#alertasstatus").html('');
                }, 5000);
      		 }
      		 $("#nuevoStatus").val('');
      		 $("#comentario").val('');
      });
    }else{
  	 $("#alertasstatus").html('<div class="alert alert-danger" role="alert">No se ha elegido un estatus nuevo...</div>');
                setTimeout(function(){
                  $("#alertasstatus").html('');
                }, 5000);
      }
    $("#tabla_pedidos").html('');
    cargarpedidos();
  });


  $("#ver_comentarios").click(function(){
  	$("#comentarios").show();
  	$("#comentarios").html('');
  	$("#DivPedidos_imprimir,#printPedidos").hide();

      $.ajax({
      	url:'view/js/admin.php?action=listarcomentariosPedidos',
      	type:'post',
      	dataType:'json',
      	data:{
      		id:$("#id_pedido").html()
      	}
      }).done(function(e){
      	for(var x=0;x<e.fecha.length;x++){
      		// alert(e.comentario[x]+"  "+e.fecha[x]);
      		$("#comentarios").append('<div class="panel panel-primary" style="border-radius: 8px 8px 0 0;color:black;background:white;margin:10px;"><div class="panel-heading" style="background:#73C6B6;border-radius: 8px 8px 0 0 ">'+e.fecha[x]+'</div> <div class="panel-body">'+e.comentario[x]+'</div> </div>');
      	}
      });
  });//fin click ver comentarios





  $("#tabla_pedidos").on('click','tr',function(){
  	var id=$(this).find('td:first-child').html();
  	// alert(id);
  	$("#modal_cambiarStatus").modal('show');
  	$("#DivPedidos_imprimir").hide();
  	$("#tb_productos_Pedidos").html("");
  	$("#id_pedido").html(id);
  });


  $("#ver_productos").click(function(){
  	$("#DivPedidos_imprimir").show();
  	$("#headerImprimir").hide();
  	$("#printPedidos").show();
  	$("#comentarios").hide();
  	$("#tb_productos_Pedidos").html('');
  	$.ajax({
        url:'view/js/admin.php?action=listarProductosPedidos',
        type:'post',
        dataType:'json',
        data:{
        	id:$("#id_pedido").html()
        }
      }).done(function(r){
      	// alert(r.status);

        	for(var x=0;x<r.cantidad.length;x++){
  			 	$("#tb_productos_Pedidos").append("<tr> <td class='d-none'>"+r.id[x]+"</td>  <td>"+r.cantidad[x]+"</td>  <td>"+r.unidad[x]+"</td>   <td>"+r.descripcion[x]+"</td>   <td>"+r.precio[x]+"</td><td>"+(parseFloat( r.cantidad[x])*parseFloat(r.precio[x]))+"</td> </tr>");
  			 }
  			 $("#importetotal").html(r.total);

  			 //print para el header
  			 $("#folio").html(r.folio);
  			 $("#Nombre").html(r.nombre);
  			 $("#fecha").html(r.fecha);
  			 $("#fechaEntrega").html(r.fecha_entregar);
  			 $("#id_operador").html(r.operador);
  			 // alert(r.operador);
  			 $("#status").html(r.status);
      });
  });//fin click

//_____________________listar historial_________________
 




  $("#back_pedidos").hide();
  $("#back_pedidos").click(function(){
    $("#seleccionarProductos").show();
      $("#info_cliente").hide();
      $(this).hide();
  });
  $("#info_cliente").hide();
    var id=[];
    var cantidad=[];
    var unidad=[];
    var descripcion=[];
    var precio=[];


     $("#pedir").click(function(){
     	$("#cliente").html('');
        servidor=$("#servidor").val();
      $("#seleccionarProductos").hide();
      $("#back_pedidos").show();
      $("#info_cliente").show();
        $("#importeFinal").val($("#importe").html());

        $("#table_agregado").find("tr td:first-child").each(function(){
        id.push($(this).text());
        cantidad.push($(this).siblings("td").eq(0).html());
        unidad.push($(this).siblings("td").eq(1).html());
        descripcion.push($(this).siblings("td").eq(2).html());
        precio.push($(this).siblings("td").eq(3).html());
    });

      ///listar clientes
      $.ajax({
        url:'view/js/admin.php?action=listarClientes',
        type:'post',
        dataType:'json'
      }).done(function(e){

        for(var x=0;x <e.id.length;x++){
          $("#cliente").append('<option value="'+e.id[x]+'">'+e.nombre[x]+'</option>');
        }
        
      });

    });//fin btn # info_cliente


     $("#table_agregado").on('keyup','tr',function(){
          var importe=0;
              $("#table_agregado").find("tr td:first-child").each(function(){
                importe=importe+parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
              });
              importe=importe.toFixed(2);
              $("#importe").html(importe);
      });


         $("#btn_enviar").click(function(){
          	$.ajax({
              url:"view/js/admin.php?action=agregarPedidos",
              type:"post",
              dataType:"json",
              data:{
                cliente:$("#cliente").val(),
                operador:$("#operador").val(),
                fecha_entregar:$("#fecha_entregar").val(),
                importe:$("#importeFinal").val(),
                id:id,
                cantidad:cantidad,
                unidad:unidad,
                descripcion:descripcion,
                precio:precio
                },error:function(){
                  $("#alertas").html('<div class="alert alert-danger" role="alert">Error al enviar, verifique la información...</div>');
                setTimeout(function(){
                  $("#alertas").html('');
                }, 8000);
                },success:function(){
                  $("#alertas").html('<div class="alert alert-success" role="alert">Enviado correctamente.</div>');
                setTimeout(function(){
                  $("#alertas").html('');
                }, 5000);
                //###########si se envia actualizar tabla de historial
                $("#tabla_historial").html('');

                  $.ajax({
                    url:'view/js/admin.php?action=listarPedidos',
                    type:'post',
                    dataType:'json'
                  }).done(function(e){
                    for(var x=0;x<e.id.length;x++){
                        $("#tabla_historial").append("<tr> <td>"+e.id[x]+"</td><td>"+e.cliente[x]+"</td><td>"+e.id_operador[x]+"</td><td>"+e.fecha_pedido[x]+"</td> <td>"+e.fecha_entregado[x]+"</td>   <td>"+e.importe[x]+"</td>   <td>"+e.status[x]+"</td></tr>");
                      }
                    });


      }
    });
      });
  //##############################FIN FOR PEDIDOS############################################################

});//fin ready$$$$$$$$$$$$$$$$$$$$$$$$

function guadarUserApp(){

  if ($("#nombre").val()!='' && $("#usuario").val()!='' &&  $("#pass").val()!='' && $("#direccion").val()!='' &&  $("#correo").val()!='' && $("#telefono").val()!='' && $("#negocio").val()!='') {
      $.ajax({
    url:'view/js/admin.php?action=guadarUserApp',
    type:'post',
    dataType:'json',
    data:{
      nombre:$("#nombre").val(),
      usuario:$("#usuario").val(),
      tipo:$("#tipo").val(),
      pass:$("#pass").val(),
      direccion:$("#direccion").val(),
      correo:$("#correo").val(),
      telefono:$("#telefono").val(),
      negocio:$("#negocio").val()
    }
  }).done(function(e){
    // alert(e.status);
    if (e.status=='true') {
      $("#modal_nuevoUserApp").modal('hide');
        $("#alerta3").html('<div class="alert alert-success" role="alert">Guardado correctamente.</div>');
              setTimeout(function(){
                $("#alerta3").html('');
              }, 5000);
              $("#nombre,#usuario,#pass,#direccion,#correo,#telefono,#negocio").val('');
    }else{
      $("#alerta3").html('<div class="alert alert-danger" role="alert">Error verifica la información...</div>');
              setTimeout(function(){
                $("#alerta3").html('');
              }, 5000);
    }
  });

  }else
  {
    $("#alerta3").html('<div class="alert alert-danger" role="alert">Existen campos vacios porfavor de rellenarlos con la información correspondientes...</div>');
              setTimeout(function(){
                $("#alerta3").html('');
              }, 5000);
  }
  $("#tb_usuariosApp tbody").html('');
  listarUserApp();
}//fin guardar user app
function listarUserApp(){
    $.ajax({
      url:'view/js/admin.php?action=listarUserApp',
      type:'post',
      dataType:'json'
    }).done(function(e){
      for(var x=0;x<e.id.length;x++){
        if (e.status[x]=='false') {
            $("#tb_usuariosApp tbody").append("<tr> <td class='d-none'>"+e.id[x]+"</td>  <td>"+e.nombre[x]+"</td>   <td>"+e.usuario[x]+"</td>  <td>"+e.direccion[x]+"</td>  <td>"+e.email[x]+"</td>  <td>"+e.telefono[x]+"</td>  <td>"+e.nombre_negocio[x]+"</td>  <td>"+e.created_at[x]+"</td> <td><i class='text-danger fa fa-circle'> </i><span class='d-none'>"+e.status[x]+"</span></td>  </tr>");
     
        }if (e.status[x]=='true') {
            $("#tb_usuariosApp tbody").append("<tr> <td class='d-none'>"+e.id[x]+"</td>  <td>"+e.nombre[x]+"</td>   <td>"+e.usuario[x]+"</td>  <td>"+e.direccion[x]+"</td>  <td>"+e.email[x]+"</td>  <td>"+e.telefono[x]+"</td>  <td>"+e.nombre_negocio[x]+"</td>  <td>"+e.created_at[x]+"</td> <td><i class='text-primary fa fa-check-circle'> </i><span class='d-none'>"+e.status[x]+"</span></td>  </tr>");
        }
       }
    });

    // $('#tb_usuariosApp').DataTable();


var listar_turno = function(){
    var table = $('#tb_usuariosApp tbody').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "view/js/admin.php?action=listarUserApp" 
        },
        "columns":[
            {"data":"nombre"},
            {"data":"usuario"},
            {"data":"direccion"},
            {"data":"email"},
            {"data":"telefono"}, 
            {"data":"nombre_negocio"},
            {"data":"created_at"}, 
            {"data":"estado",
            render:function(data, type, row){
                if (data==1) {
                  return "<p class='text-primary'>CERRADO</p>";  
                }if (data==0) {
                    return "<p class='text-danger'>ABIERTO</p>"; 
                }
                }
            }    
        ]
    });
}
























}
function agregar(id,unidad,nombre,precio){
    var existe=0;
    $("#table_agregado").find("tr td:first-child").each(function(){
      var id_pro=$(this).text();
        if (id_pro==id) {
          existe=1;
          $(this).siblings("td").eq(0).html(parseInt($(this).siblings("td").eq(0).html())+1);
          var importe=0;
            $("#table_agregado").find("tr td:first-child").each(function(){
              importe=importe+parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
            });
            importe=importe.toFixed(2);
            $("#importe").html(importe);
        }
    });//fin each 
    if (existe==0) {
      var importe=0;
      $("#table_agregado").append("<tr> <td style='display: none'>"+id+"</td><td contenteditable='true'>1</td><td>"+unidad+"</td> <td>"+nombre+"</td><td>"+precio+"</td></tr>");
     
      $("#table_agregado").find("tr td:first-child").each(function(){
        importe=importe+parseFloat($(this).siblings("td").eq(0).html())*parseFloat($(this).siblings("td").eq(3).html());
      });
    importe=importe.toFixed(2);
      $("#importe").html(importe);
    }
}//fin agregar
function buscarProductos(){
  var filtro=$("#filtro").val();
  servidor=$("#servidor").val();
  $.ajax({
    url:"view/js/buscarproducto.php?action=buscarProducto",
    type:"post",
    dataType:"json",
    data:{producto:filtro}
  }).done(function(e){
    $("#tablaBusqueda").html("");
    for(var x=0;x<e.id_art.length;x++){
      // alert(e.id_art[x]);
      $("#tablaBusqueda").append("<tr><td style='display: none'>"+e.id_art[x]+"</td><td>"+e.unidad[x]+"</td> <td>"+e.nombre[x]+"</td>    <td>"+e.precio[x]+"</td><td><button class='btn btn-info btn-sm' onclick='agregar("+e.id_art[x]+","+'"'+e.unidad[x]+'"'+","+'"'+e.nombre[x]+'"'+","+e.precio[x]+")'><i class='fa fa-plus-circle'></i></button></td>  </tr>");
    }
    
  });
}
function cargarpedidos(){
	$.ajax({
        url:'view/js/admin.php?action=listarPedidosAdmin',
        type:'post',
        dataType:'json'
      }).done(function(e){
        for(var x=0;x<e.id.length;x++){
        	if (e.status[x]=='pendiente') {
        		$("#tabla_pedidos").append("<tr> <td class='d-none'>"+e.id[x]+"</td><td>"+e.cliente[x]+"</td><td>"+e.fecha_pedido[x]+"</td> <td>"+e.fecha_entregado[x]+"</td>   <td>"+e.importe[x]+"</td>   <td><i class='fa fa-circle text-secondary'></i> "+e.status[x]+"</td></tr>");

        	}
        	if (e.status[x]=='proceso') {
        		$("#tabla_pedidos").append("<tr> <td class='d-none'>"+e.id[x]+"</td><td>"+e.cliente[x]+"</td><td>"+e.fecha_pedido[x]+"</td> <td>"+e.fecha_entregado[x]+"</td>   <td>"+e.importe[x]+"</td>   <td><i class='fa fa-circle text-warning'></i> "+e.status[x]+"</td></tr>");

        	}
        	if (e.status[x]=='enviado') {
        		$("#tabla_pedidos").append("<tr> <td class='d-none'>"+e.id[x]+"</td><td>"+e.cliente[x]+"</td><td>"+e.fecha_pedido[x]+"</td> <td>"+e.fecha_entregado[x]+"</td>   <td>"+e.importe[x]+"</td>   <td><i class='fa fa-circle text-primary'></i> "+e.status[x]+"</td></tr>");

        	}
        	if (e.status[x]=='recibido') {
        		$("#tabla_pedidos").append("<tr> <td class='d-none'>"+e.id[x]+"</td><td>"+e.cliente[x]+"</td><td>"+e.fecha_pedido[x]+"</td> <td>"+e.fecha_entregado[x]+"</td>   <td>"+e.importe[x]+"</td>   <td><i class='fa fa-circle text-success'></i> "+e.status[x]+"</td></tr>");

        	}
          }//fin for
           $('#tablaPedidos').DataTable();
        });
}


