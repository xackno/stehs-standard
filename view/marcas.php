<div class="page-content-header">
    <div class="btn-group pull-right">
        <button type="reset" href="#Modal" class="btn btn-sm btn-dark" data-toggle="modal" >Nuevo</button>
    </div>
    <h3>
        <i class="zmdi zmdi-map" style="color: red;"></i></i>
        Marcas
    </h3>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="box box-blue">
                <div class="box-body">
                    <div class="table_responsive">
                      <table id="tab_marcas" class="table table-bordered display" >
                           <thead>
                               <tr>
                                    <td>Nombre</td>
                                    <td>Acciones</td>
                               </tr>
                           </thead>
                       </table>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model content from here -->
       

        <div class="modal fade" id="Modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style=" align-items: center;">
                    <div class="modal-header">
                        <h6 class="modal-title" style="text-align: center;">Registro de marcas</h6>
                    </div>
                    <div class="modal-body">
                        <form id="form1" method="post" onsubmit="return marcas();">
                            <div class="form-group">
                                <label for="nombre_marca" class="form-control-label">Nombre:</label>
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
                                    <input type="text" name="nombre_marca" class="form-control" id="nombre_marca">
                                </div>
                            </div>
                   
           
                            <div class="modal-footer">
                                <button type="submit" name="enviar" class="btn btn-primary" >GUARDAR</button>
                                <button type="reset" class="btn btn-warning">BORRAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div
        </div>
