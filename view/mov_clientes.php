<?php
$id=$_GET['id_cliente'];

  require("model/conexion_mysqli.php");

  $acentos = $conexion->query("SET NAMES 'utf8'");
  $result = $conexion->query($acentos);
  $enviar=new stdClass(); 

  //########EXTRAYENDO DATOS DEL CLIENTE############
  $consulta = "select  *FROM clientes where id_cliente='$id' ";
  $result = $conexion->query($consulta);
  while($fila = $result->fetch_array()){
    $nombre_cliente= $fila['nombre_cliente'];
    $domicilio_cliente= $fila['domicilio_cliente'];
    $colonia_cliente=$fila['colonia_cliente'];
    $telefono_cliente= $fila['telefono_cliente'];
    $credito= $fila['credito'];
  }

?>



<div class="ui-content" style="margin: 30px">
  <div class="page-content-header">
        <h3>
            <i class="zmdi zmdi-book"></i>
        <small>Movimientos del cliente <?php echo $_GET['id_cliente']?></small>
        </h3>
    </div>
  <div id="seccionImprimir">
    <div id="headerPrint" style="width: 100%;display: none" >
      <img src="view/img/headerMov.png" style="width: 100%">
    </div>
      <label id="nombre">Nombre:<b> <?php echo $nombre_cliente; ?> </b></label>
      <label id="domicilio" style="float: right;">Domicilio: <?php echo $domicilio_cliente." ".$colonia_cliente;?></label>
      <br>
      <label id="telefono">Telefono: <?php echo $telefono_cliente;?></label>
      <label id="credito" style="float: right;color: red;">Credito actual: $<?php echo $credito;?></label>

          <div class="table_responsive">
            <table id="tabla_historial" style="font-size: 70%" class="table table-bordered display" >
                 <thead>
                     <tr>
                        <th >Tipo</th>
                        <th># Ticket</th>
                        <th>Fecha</th>
                        <th>F.Limite</th>
                        <!-- <th>Cant</th> -->
                        <th>Productos</th>
                        <!-- <th>Precio</th> -->
                        <th>Importe</th>
                        <th>Pago inicial</th>
                        <th>Saldo</th>
                     </tr>
                 </thead>
                 <tbody style="font-size: 150%">
                  <?php

  
              //########EXTRAYENDO DATOS DEL CREDITO DEL CLIENTE########
              $consulta2 = "select  *FROM mov_cliente INNER JOIN creditos ON mov_cliente.id_tipo=creditos.id_credito where mov_cliente.id_cliente=$id ";
              $result2 = $conexion->query($consulta2);
              $total_decreditos=0;
              $total_deabonos=0;
              $total_depagoinicial=0;
              $credito_actual=0;
              
                
              while($fila = $result2->fetch_array()){
                $id_tipo=$fila['id_tipo'];
                $id_venta=0;

                if ($fila['tipo']=="credito") {
                    $consulta3 = "select  *FROM creditos where id_credito=$id_tipo";
                    $result3 = $conexion->query($consulta3);
                    while($fila2 = $result3->fetch_array()){
                          $fecha_limite=$fila2['fecha_limite'];
                          $pago_inicial=$fila2['pago_inicial'];
                          $total_decreditos+=$fila2['total_credito'];
                          $total_depagoinicial+=$fila2['pago_inicial'];
                          $credito_actual=$fila['saldo'];
                          $id_venta=$fila2['id_venta'];
                    }
                  ?>
                  <tr> 
                    <td style="color:blue "><?php echo $fila['tipo']; ?></td>
                    <td ><?php echo $fila['id_venta']; ?></td>
                    <td><?php echo $fila['fecha']; ?></td>
                    <td><?php echo $fecha_limite ?></td>
                    <td id="td<?php echo $fila['id_tipo']?>"><button class="btn btn-info" onclick="verproductoscomprados(<?php echo $fila['id_tipo']; ?>);"><i class="fa fa-eye"></i></button>
                        
                     </td>
                    <td><?php echo $fila['importe']; ?></td>
                    <td><?php  echo $pago_inicial; ?></td>
                    <td><?php echo $fila['saldo'];?></td>
                    
                  </tr>
                <?php
                }//fin if credito
                if ($fila['tipo']=="abono") {
                    $consulta3 = "select  *FROM abonos where id_abono=$id_tipo";
                    $result3 = $conexion->query($consulta3);
                    while($fila2 = $result3->fetch_array()){
                          $tipo_abono=$fila2['tipo'];
                         $credito_actual=$fila2['nuevo_credito'];
                         $total_deabonos+=$fila2['cantidad'];
                    }
                  ?>
                  <tr> 
                    <td style="color:red "><b><?php echo $fila['tipo']; ?> </b></td>
                    <td><?php echo $fila['id_tipo']; ?></td>
                    <td><?php echo $fila['fecha']; ?></td>
                    <td><?php ?></td>
                    <td style="color:green"> <?php echo $tipo_abono; ?></td>
                    <td><?php echo $fila['importe']; ?></td>
                    <td><?php ?></td>
                    <td><?php echo $fila['saldo'];?></td>
                    
                  </tr>
                <?php
                }
                
              }
              ?>



          
                 </tbody>
             </table>
                <div style="float: right;">
                  <h4>Total de créditos:$ <?php echo $total_decreditos;?></h4>
                  <h4>total de pago inicial:$ <?php echo $total_depagoinicial;  ?></h4>
                  <h4>Total de abonos:$ <?php echo $total_deabonos;?> </h4>
                  <h4>Crédito actual:$ <?php echo $credito;?></h4>
                </div>
                <br><br><br><br><br>
         
          </div>
          </div>
          
          <button class="btn btn-success"  id="crearPDF">Imprimir PDF</button>

</div>





















