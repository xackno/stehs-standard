
<div class="container-fluid">

  <div class="card">
  <div class="card-header bg-primary text-white">
    <h4><i class="fa fa-clipboard"></i> Historial de movimiento de inventario</h4>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="input-group">
          <input type="date" id="date" class="form-control">
          <button class="btn btn-success" id="buscar_fecha"><i class="fa fa-search"></i></button>
        </div>
      </div>
      <div class="col-xl-4 col-md-6 mb-4"></div>
      <div class="col-xl-4 col-md-6 mb-4"></div>
    </div>
      <div class="table-responsive">
        <table class="table table-sm" id="tab_historial_movimiento">
          <thead class="bg-danger">
            <th>ID</th>
            <th>Fecha-Hora</th>
            <th>Id producto</th>
            <th>Descripción</th>
            <th>Movimiento</th>
            <th>Tipo</th>
            <th>cantidad</th>
            <th>Cajero</th>
          </thead>
          <tbody>
            
          </tbody>
          
        </table>
      </div>
  </div>
</div>



</div>
<!-- model content from here -->
       





