<div class="page-content-header">
    <div class="btn-group pull-right">
        <button type="reset" href="#Modal" class="btn btn-sm btn-primary" data-toggle="modal" >Nuevo</button>
    </div>
    <h3>
        <i class="zmdi zmdi-map" style="color: red;"></i>
        Pedidos
    </h3>
</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="box box-blue">
                    <div class="box-body">
                        <div class="table-responsive">
                          <table class="table table-bordered"  id="tablaPedidos" width="100%" cellspacing="0">
                            <thead class="table" style="background: #3D9F5A ">
                              <tr>
                                <th class='d-none'>folio</th>
                                <th>cliente</th>
                                <!-- <th>operador</th> -->
                                <th>fecha de pedido</th>
                                <th>fecha de entregado</th>
                                <th>importe</th>
                                <th>Estatus</th>
                              </tr>
                            </thead>
                            <tbody id="tabla_pedidos">


                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- model content from here -->
       

<div class="modal fade" id="Modal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style=" align-items: center;">
                    <div class="modal-header">
                        <h6 class="modal-title" style="text-align: center;">Nuevo pedido</h6>
                    </div>
                    <div class="modal-body" style="width: 100%">
                         <div id="alertas">   
                        </div>
                       

                          <div id="pedidos">
                            <button id="back_pedidos" style="float: left;" class="btn btn-info btn-circle"><i class="  fa fa-arrow-circle-left"></i></button><br><br>
                                       <div id="seleccionarProductos" style="width: 100%;padding: 0px;margin: 0px">
                                          
                                              <h5 >Buscar productos</h5>
                                                  <div class="input-group">

                                                    <input type="text" class="form-control " placeholder="Buscar..." aria-label="Search"  id="filtro">
                                                    <div class="input-group-append">
                                                      <button class="btn btn-primary" type="button" onclick="buscarProductos()">
                                                        <i class="fa fa-search "></i>
                                                      </button>
                                                      
                                                    </div>
                                                  </div>
                                               <br><br>
                                          
                                          
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                  <thead class="table table-dark" style="color: black;background: #3D9F5A;width: 100%">
                                                    <tr>
                                                      <th class="d-none">id</th>
                                                      <th>Unidad</th>
                                                      <th>Descripción</th>
                                                      <th>precio</th>
                                                      <th>Conf</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="tablaBusqueda">


                                                  </tbody>
                                                </table>
                                              </div>
                                        <style type="text/css">
                                          table{font-size: 100%;}
                                          table tbody{color: black}
                                          .btn-circle {
                                              width: 30px;
                                              height: 30px;
                                              padding: 6px 0px;
                                              border-radius: 15px;
                                              text-align: center;
                                              font-size: 12px;
                                              line-height: 1.42857;
                                          }
                                        </style>
                                          <h3>Productos a pedir</h3>
                                          <div class="table-responsive">
                                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                              <thead class="table table-dark" style="color: black;background: #3D9F5A;width: 100%"> 
                                                <tr>
                                                  <th class="d-none">id</th>
                                                  <th>cantidad</th>
                                                  <th>Unidad</th>
                                                  <th>Descripción</th>
                                                  <th>precio</th>
                                                </tr>
                                              </thead>
                                              <tbody id="table_agregado">


                                              </tbody>
                                            </table>
                                          </div>
                                          <br>
                                          <p class="text-success" style="font-size:120%;float: right;" >Importe: $ <span class="text-danger" id="importe"></span></p>
                                          
                                        <br><br>
                                        <button class="btn btn-success" style="float: right;" id="pedir">Pedir</button>
                                    </div>

                                    <!-- Fin seleccionar productos -->
                                    <div id="info_cliente">
                                      <label>Cliente</label>
                                        <select id="cliente" class="form-control">
                                            
                                        </select>
                                        
                                        <br>
                                        <label>Fecha de entrega</label>
                                        <input type="date" name="" class="form-control" id="fecha_entregar">
                                        <label>Importe</label>
                                        <input type="number" name="" class="form-control" id="importeFinal" readonly="">
                                        <br><br>
                                        <button class="btn btn-sm btn-primary" id="btn_enviar" style="float: right;">Enviar</button>
                                    </div>
                          </div>





                    </div>
                </div>
            </div>
</div>









<!-- $$$$$$$$$$$$$$$modal cambiar status###################### -->
  <div class="modal fade" id="modal_cambiarStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white">
          <h3><i class='fa fa-circle text-danger'></i> Cambiar estado <span id="id_pedido" class="d-none"></span></h3>
        </div>
        <div class="modal-body">
          <div id="alertasstatus">   
          </div>

              <select class="select form-control " id="nuevoStatus" required="true">
                <option value="0" selected="true" disabled="true">Seleccione...</option>
                <option value="pendiente" class="text-secondary">Pendiente </option>
                <option value="proceso" class="text-warning"> En proceso</option>
                <option value="enviado" class="text-primary">Enviado</option>
                <option value="recibido" class=" text-success">recibido</option>
              </select>
              <br><br>
              <textarea id="comentario" placeholder="Agregar una breve comentario" style="width: 100%"></textarea>
              <br><br>
              <div class="row">
                <div class="col-4">
                    <button class="btn btn-primary " id="ver_productos" style="float: left;">Ver Productos</button>
                </div>
                <div class="col-4">
                    <button class="btn btn-warning" id="ver_comentarios">Ver comentario</button>
                </div>
                <div class="col-4">
                  <button class="btn btn-success" id="cambiarstatus"  style="float: right;">Cambiar</button>
                </div>
              </div>
              <br><br>
              
              <div id="DivPedidos_imprimir">
                  <div id="headerImprimir" style="background: url(view/img/header.jpg);">
                    <div class="row">
                      
                      <div class="col-4">

                        <p style="background: white">Nombre:<b><span id="Nombre"></span></b></p>
                        <p style="background: white">Fecha de solicitud:<span id="fecha"></span></p>
                        <p style="background: white">Fecha de entrega:<span id="fechaEntrega"></span></p>
                      </div>
                      <div class="col-4">
                        
                      </div>
                      <div class="col-4">
                        <p style="background: white">FOLIO:<span id="folio" class="text-danger"></span></p>
                        <p style="background: white">Operado:<span id="id_operador"></span></p>
                         <p style="background: white">Estado:<span id="status"></span></p>
                      </div>

                    </div>
                    
                  </div>
                  <div id="Div_Productos">
                      <div class="table-responsive">
                          <table class="table table-bordered"  width="100%" cellspacing="0">
                            <thead class="table table-dark" style="color: black;background: #3D9F5A;width: 100%"> 
                              <tr>
                                <th class="d-none">id</th>
                                <th>cantidad</th>
                                <th>Unidad</th>
                                <th>Descripción</th>
                                <th>precio</th>
                                <th>total</th>
                              </tr>
                            </thead>
                            <tbody id="tb_productos_Pedidos">


                            </tbody>
                          </table>
                        <span class="text-danger" style="font-size: 120%;float: right;">Total=$<span id="importetotal"></span></span>
                        </div>
                        <br><br>
                    
                  </div>
              </div>
              <button class="btn btn-info" id="printPedidos" style="float: right;">Imprimir</button>
              
              <div id="comentarios" style="width: 100%;background:#5D6D7E;height: 250px; overflow-y: scroll;"><h5 class="text-light">Comentarios</h5>
                <br>
              </div>
        </div>
      </div>
    </div>
  </div>
<!-- fin modal cambiar status -->


