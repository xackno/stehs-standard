<div class="page-content-header">
    <div class="btn-group pull-right">
        <!-- <b><a href="./?view=form_alumno&accion=nuevo" class="btn btn-secondary btn-sm"><i class="fa fa-file-text-o fa-lg"></i></a></b> -->
        <button type="button" href="#Modal" class="btn btn-sm btn-dark" data-toggle="modal" >Nuevo</button>
    </div>
    <h3>
        <i class="zmdi zmdi-truck" style="color: blue;"></i>
        Proveedores
        <!-- <small>Listados</small> -->
        <i class="zmdi zmdi-male" style="color: orange;"></i>
    </h3>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="box box-blue">
                <div class="box-body">
                    <div class="table_responsive">
                      <table id="tab_proveedor" class="table table-bordered display">
                           <thead>
                               <tr>
                                    <td>Nombre</td>
                                    <td>Telefono</td>
                                    <td>Dirección</td>
                                    <td>Acciones</td>
                               </tr>
                           </thead>
                       </table>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model content from here -->
       

        <div class="modal fade" id="Modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style=" align-items: center;">
                    <div class="modal-header">
                        <h6 class="modal-title" style="text-align: center;">Registro de proveedores</h6>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>
                    <div class="modal-body">
                        <!-- <form action="comprueba_login.php" method="post"> -->
                        <form id="form" method="post" onsubmit="return proveedor();">
                        <!-- <form action="controller/proveedor.php?action=guardar" method="post"> -->
                            <div class="form-group">
                                <label for="nombre_proveedor" class="form-control-label">Nombre:</label>
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
                                    <input type="text" name="nombre_proveedor" class="form-control" id="nombre_proveedor">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telefono_proveedor" class="form-control-label">Telefono:</label>
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-addon"><i class="fa fa-phone fa-lg"></i></div>
                                    <input type="text" name="telefono_proveedor" class="form-control" id="telefono_proveedor">
                                 </div>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="form-control-label">Dirección:</label>
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
                                    <input type="text" name="direccion" class="form-control" id="direccion">
                                 </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="enviar" class="btn btn-primary">GUARDAR</button>
                                <button type="reset" class="btn btn-warning">BORRAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>