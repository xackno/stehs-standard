<div  class="container-fluid">
  <div class="card">
    <div class="card-header" style="background: #2ECC71">
      <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
          <h2 class="text-white"><i class="fa fa-clipboard"></i> Reportes</h2>
        </div>
        <div class="col-xl-4 col-md-6 mb-4"></div>
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="button-group">
            <button class="btn text-white" id="MostrarProVendidos" style="background: #8E44AD">Productos vendidos</button>
            <button class="btn btn-primary" id="mostrarHstlVenta">Ventas</button>
            <button class="btn btn-danger" id="MostrarHstlTurno">Turnos</button>
            <button class="btn text-white btn-info" id="btn_a_salidas">Salidas</button>
            <a class="btn btn-dark" href="./?view=estado">ESTADO</a>
          </div>
        </div>
      </div>
      
        
    </div>
    <div class="card-body">
        <div id="historialVentas">
          <div class="row">
              <div class="col">
                  <div class="table_responsive">
                    <h3><span class="badge badge-danger">Tabla de ventas</span></h3>
                    <table id="tab_venta" class="table table-bordered display" >
                        <thead>
                          <tr>
                            <td>Id venta</td>
                            <td>Pago</td>
                            <td>Total_venta</td>
                            <td>Cambio</td>
                            <td>Fecha</td>
                            <td>Tipo</td>
                            <td>articulos vendidos</td>
                          </tr>
                        </thead>
                      </table>      
                  </div>
              </div>
          </div>
      </div>
      <!-- ########################################################## -->
      <!-- #################HISTORIA DE TURNOS######## -->
       <div  id="historialTurnos">
          <div class="table_responsive">
            <h3><span class="badge badge-danger">Tabla de turnos</span></h3>
            <table id="tab_turno" class="table table-bordered display" >
                 <thead >
                     <tr>
                          <th>Id turno</th>
                          <th><i class="fa fa-user"></i></th>
                          <th>Fecha inicio</th>
                          <th>fecha Finalizado</th>
                          <th>$ inicio</th>
                          <th>$ Total en caja</th>
                          <th>$ Diferencia</th>
                          <th>Comentario al inicio</th>
                          <th>Comentario al cierre</th>
                          <th>Estatus</th>
                         
                     </tr>
                 </thead>
             </table>      
          </div>
      </div>

    <!-- #################Ver productos vendidos del dia######## -->
     <div id="productosVendidos">
        <div class="row">
          <div class="col-3"> 
            <h3><b>Seleccione fecha</b> </h3>
            <div class="input-group">
              <input type="date" name="primerFecha" id="primerFecha" class="form-control">
              <button class="btn btn-primary" id="verProductos_del_dia"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      
        <div class="table_responsive" id="printTablaProdVendidos">
          <h3><span class="badge badge-danger">Tabla de productos vendidos</span></h3>
          <table id="tab_produtosVendidos" class="table table-bordered display" >
               <thead style="font-size: 75%">
                   <tr>
                    <!-- <td>Usuario</td> -->
                        <th>Id</th>
                        <th>cantidad</th>
                        <th>Unidad</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Importe</th>
                        <th>Fecha-hora</td> 
                   </tr>
               </thead>
               <tbody>
                 
               </tbody>
           </table>        
        </div>
        <button class="btn btn-success" id="printProductoVendidos">Imprimir en pdf</button>    
    </div>
<!-- #################Ver SALIDAS################################### -->
     <div id="card_salidas">
      <div class="row">
          <div class="col-3"> 
            <h3><b>Seleccione fecha</b> </h3>
            <div class="input-group">
              <input type="date" name="primerFecha" id="Fecha_busqueda" class="form-control">
              <button class="btn btn-primary" id="ver_salidas_del_dia"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
    
        <div class="table_responsive">
          <h3><span class="badge badge-danger">Tabla de salidas de efectivos</span></h3>
          <table id="tab_list_salidas" class="table table-bordered display" >
               <thead style="font-size: 75%">
                   <tr>
                        <th>Id</th>
                        <th>turno</th>
                        <th>Usuario</th>
                        <th>Concepto</th>
                        <th>Cantidad</th>
                        <th>Fecha</th>
                   </tr>
               </thead>
               <tbody>
                 
               </tbody>
           </table>        
        </div>
    </div>





    </div>
  </div><!-- fin card -->
</div><!-- fin container -->




