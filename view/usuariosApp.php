<div class="page-content-header">
    <div class="btn-group pull-right">
        <button type="reset" href="#modal_nuevoUserApp" class="btn btn-sm btn-primary" data-toggle="modal" >Nuevo</button>
    </div>
    <h3>
        <i class="fa fa-user" style="color: red;"></i>
        Usuarios para pedidos <i class="text-primary fa fa-check-circle"> </i>
    </h3>
</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="box box-blue">
                    <div class="box-body">
                        <div class="table-responsive">
                          <table class="table table-bordered"  id="tb_usuariosApp" width="100%" cellspacing="0">
                            <thead class="tabl" style="background: #3D9F5A ">
                              <tr>
                                <th class='d-none'>id</th>
                                <th>nombres</th>
                                <th>usuario</th>
                                <th>dirección</th>
                                <th>email</th>
                                <th>teléfono</th>
                                <th>Negocio</th>
                                <th>creado</th>
                                <th>Validado</th>

                              </tr>
                            </thead>
                            <tbody >


                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- model content from here -->







<!-- $$$$$$$$$$$$$$$modal ###################### -->
  <div class="modal fade" id="modal_nuevoUserApp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="fa fa-user" style="color:#FF336F"></i>Nuevo Usuario</h3>
        </div>
        <div class="modal-body">
            <div id="alerta3"></div>
                <label>Nombre</label>
            <input type="text" class="form-control" id="nombre" placeholder="Nombre y apellidos" required="">
            <label>Usuario</label>
            <input type="text" class="form-control" id="usuario" placeholder="" required="">
            <div class="form-group">
              <label for="tipo">Tipo</label>
              <select id="tipo" class="form-control text-success" >
                <option selected="true" disabled="true">Seleccione unma opción</option>
                <option value="administrador">Administrador</option>
                <option value="repartidor">Repartidor</option>
                <option value="chofer">Chofer</option>
              </select>
            </div>
            <label>Contraseña</label>
            <input type="password" class="form-control" id="pass" required="">
            <label>Dirección</label>
            <input type="text" class="form-control" id="direccion" placeholder="Ser específico..." required="">
            <label>Correo</label>
            <input type="email" class="form-control" id="correo" required="">
            <label>Telefono</label>
            <input type="tel" class="form-control"  id="telefono" required="">
            <label>Nombre del negocio</label>
            <input type="text" class="form-control" id="negocio" required="">
            <br>
            <input type="submit" class="btn btn-success"  style="float: right;" type="submit" onclick="guadarUserApp();" value="Crear">
            
              
        </div>
      </div>
    </div>
  </div>
<!-- fin modal  -->

<!-- $$$$$$$$$$$$$$$modal al click en tr de la tablausuariosApp###################### -->
  <div class="modal fade" id="modal_User" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="fa fa-user" style="color:#FF336F"></i> <span id="idUserApp" class="d-none"></span> <span id="nombreUsuario"></span></h3>
        </div>
        <div class="modal-body">
            <div id="alertasactivar"></div>
                <!-- Rounded switch -->
                <label><b>Validar</b></label><br>
                <label class="switch">
                  <input type="checkbox" id="activar" >
                  <span class="slider round"></span>
                </label>

            <button class="btn btn-success" id="activarCuenta" style="float: right;" >Guardar</button>
            
              
        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->

<style type="text/css">
    

    /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>




