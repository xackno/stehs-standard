$("#wrapper").toggleClass("toggled");
$(document).ready(function (){
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    listar_proveedor();
    listar_clientes();
    listar_marcas();
    listar_venta();
    listar_articulos();   
    listar_turno(); 
    listar_historial_movimiento();
    // listar_salidas();
    list_salidas();
    // list_pro_vend();
});

$("#buscar_fecha").click(function(){
    var table = $('#tab_historial_movimiento').DataTable();
    var date=$("#date").val();
     table.search(date).draw();
});
$("#ver_salidas_del_dia").click(function(){
    var table = $('#tab_list_salidas').DataTable();
    var date=$("#Fecha_busqueda").val();
     table.search(date).draw();
});

// var list_pro_vend = function(){
//     var table = $('#tab_produtosVendidos').DataTable({
//         "destroy": true,
//         "ajax":{
//             method: "post",
//             url: "controller/proveedor.php?action=listar_pro_vend" 
//         },
//         "columns":[
//             {"data":"venta"},
//             {"data":"cantidad_p"},
//             {"data":"unidad"},
//             {"data":"descripcion_articulo"},
//             {"data":"precio_pro"},
//             {"data":"monto_p"}
//         ]
//     });
// }






var list_salidas = function(){
    var table = $('#tab_list_salidas').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/proveedor.php?action=listarsalidas" 
        },
        "columns":[
            {"data":"id_salida"},
            {"data":"id_turno"},
            {"data":"usuario"},
            {"data":"concepto"},
            {"data":"cantidad"},
            {"data":"fecha"}

        ]
    });
}





var listar_historial_movimiento = function(){
    var table = $('#tab_historial_movimiento').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/proveedor.php?action=listar_historial_movimiento" 
        },
        "columns":[
            {"data":"id"},
            {"data":"fecha"},
            {"data":"id_articulo"},
            {"data":"descripcion_articulo"},
            {"data":"movimiento"},
            {"data":"tipo",
                render:function(data){
                    if (data=="Devolución") {
                         return "<span class='badge badge-warning'>"+data+" <i class='   fa fa-hand-o-right'></i></span>";
                    }if (data=="Entrada") {
                        return "<span class='badge badge-success'>"+data+" <i class='   fa fa-hand-o-right'></i></span>";
                    }if (data=="Ajuste") {
                        return "<span class='badge badge-info'><i class='fa fa-chevron-left'></i> "+data+" <i class='fa fa-chevron-right'></i></span>";
                    }
                }
            },
            {"data":"cantidad"},
            {"data":"cajero"},  
        ]
    });
}






var listar_turno = function(){
    var table = $('#tab_turno').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/proveedor.php?action=listar_turno" 
        },
        "columns":[
            {"data":"id_turno"},
            {"data":"usuario"},
            {"data":"fecha_inicio"},
            {"data":"fecha_fin"},
            {"data":"inicio"}, 
            {"data":"cierre"},
            {"data":"diferencia",
                render:function(data,type,row){
                    if (data>0) {
                        return "<span class='badge badge-success badge-pill'>Ganancia por<br> $"+data+"</span>";
                    }if(data==0){
                        return "<span class='badge badge-warning badge-pill '>"+data+"</span>";    
                    }if(data<0){
                        var data=data.replace("-",'');
                        return "<span class='badge badge-danger badge-pill'>Perdido por<br> $"+data+"</span>";    
                    }
                    
                }
            },
            {"data":"comentario"}, 
            {"data":"comentarioCierre"},
            {"data":"estado",
            render:function(data, type, row){
                if (data==1) {
                  return "<p class='badge badge-primary'>CERRADO</p>";  
                }if (data==0) {
                    return "<p class='badge badge-warning'>ABIERTO</p>"; 
                }
                }
            }    
        ]
    });
}



var listar_proveedor = function(){
    var table = $('#tab_proveedor').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/proveedor.php?action=listar" 
        },
        "columns":[
            {"data":"nombre_proveedor"},
            {"data":"telefono_proveedor"},
            {"data":"direccion"},
            {"data":"id_proveedor",
                render:function(data, type, row){
                    return "<a href='?view=form_proveedor&accion=editar&id_proveedor="+data+"' class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a><button class='btn btn-sm btn-danger' onclick='eliminarPro("+data+");'><i class='fa fa-trash-o'></i></button>"

                }}    
        ]
    });
}

var listar_articulos = function(){
    var table = $('#tab_articulos5').DataTable({
            "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/articulos.php?action=listar" 
        },
        "columns":[
            {"data":"clave_articulo"},
            {"data":"descripcion_articulo"},
            {"data":"unidad"},
            {"data":"codigo_barra"},
            {"data":"precio_c"},
            {"data":"precio_p"},
            {"data":"nombre_marca"},
            {"data":"cantidad",
            render:function(data,type,row){
                if ($("#tipo_usuario_log").val()=="admin"){
                    return data;
                }else{
                    return "<span style='font-size:70%' class='text-danger'>solo administrador</span>";
                }
            }

            },
            {"data":"id_articulo",
                render:function(data, type, row){
                    if ($("#tipo_usuario_log").val()=="admin"){
                        return "<a href='?view=form_articulos&accion=editar&id_articulo="+data+"' class='btn btn-sm btn-primary horiz'><i class='fa fa-edit'></i></a><button class='btn btn-sm btn-danger horiz' onclick='eliminarArt("+data+");'><i class='fa fa-trash-o'></i></button><a href='?view=form_articulos&accion=listar&id_articulo="+data+"' class='btn btn-sm btn-success horiz'>SAT</a>";
                    }//fin if 
                    else{
                        return "<button class='btn btn-primary btn-sm' disabled='true'><i class='fa fa-edit'></i></button><button class='btn btn-sm btn-danger horiz' disabled='true'><i class='fa fa-trash-o'></i></button><button class='btn btn-success btn-sm' disabled='true'>SAT</button>";

                    }
                }
            } 
        ]
    });
}

//------------listar clientes ----------------//
var listar_clientes = function(){
    var table = $('#tab_clientes').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/clientes.php?action=listar" 
        },
        "columns":[
            {"data":"nombre_cliente"},
            {"data":"domicilio_cliente"},
            {"data":"colonia_cliente"},
            {"data":"municipio_cliente"},
            {"data":"cp_cliente"},
            {"data":"telefono_cliente"},
            {"data":"credito"},
            {"data":"id_cliente",
                render:function(data, type, row){
                     if ($("#tipo_usuario_log").val()=="admin"){
                    return "<a href='?view=form_clientes&accion=editar&id_cliente="+data+"' class='btn btn-sm btn-primary edit_client'><i class='fa fa-edit'></i></a><button class='btn btn-sm btn-danger delete_client' onclick='eliminarClientes("+data+");'><i class='fa fa-trash-o'></i></button>  <a href='?view=mov_clientes&accion=verMov&id_cliente="+data+"' class='btn btn-sm btn-success'><i class='fa fa-eye'></i></a>"

                     }else{
                        return "<button disabled='true' class='btn btn-sm btn-primary edit_client'><i class='fa fa-edit'></i></button><button class='btn btn-sm btn-danger delete_client' disabled='true'><i class='fa fa-trash-o'></i></button> <a href='?view=mov_clientes&accion=verMov&id_cliente="+data+"' class='btn btn-sm btn-success'><i class='fa fa-eye'></i></a>"
                     }

                }}    
        ]
    });
}
var listar_marcas = function(){
    var table = $('#tab_marcas').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/marcas.php?action=listar" 
        },
        "columns":[
            {"data":"nombre_marca"},
            {"data":"id_marca",
                render:function(data, type, row){
                    return "<a href='?view=form_marcas&accion=editar&id_marca="+data+"' class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>"

                }}    
        ]
    });
}
var listar_venta = function(){
    var table = $('#tab_venta').DataTable({
        "destroy": true,
        "ajax":{
            method: "post",
            url: "controller/venta.php?action=listar" 
        },
        "columns":[
            {"data":"id_venta"},
            {"data":"pago"},
            {"data":"total_venta"},
            {"data":"cambio"},
            {"data":"fecha"},
            {"data":"a_credito", 
            render:function(data, type, row){
                if (data==1) {
                    return "<p class='text-success' style='color: white'><i class='zmdi zmdi-card'></i>A crédito</p>";
                }else{
                    return "<p class='text-info' style='color: white'> <i class='zmdi zmdi-money'></i>Al contado</p>";
                }
            }},
            {"data":"id_venta",
                render:function(data, type, row){

                    return "<a href='?view=form_artvendidos&id_venta="+data+"'   class='btn btn-sm btn-primary'><i class='zmdi zmdi-eye'></i></a>"

                }}
        ]
    });
}
//eliminar
var eliminarPro = function(id_proveedor){
    var msj=confirm("¿EN VERDAD DESEA ELIMINAR ESTE PROVEEDOR?");
    if (msj) {
        $.ajax({
        method: "post",
        url: "controller/proveedor.php?action=eliminar",
        data: {id_proveedor: id_proveedor},
        success: function(r){
            if(r == 1){
                alert("eliminado");
                listar_proveedor();
            }else{
                alert("errorcillo");
                // alert(e);
            }
        }
    });
    }  
}
var eliminarPreventas = function(id_preventa){
    $.ajax({
        method: "post",
        url: "controller/preventa.php?action=eliminar",
        data: {id_preventa: id_preventa},
        success: function(r){
            if(r == 1){
                alert("eliminado");
                // listar_preventa();
                $('#load_preventa').load('view/table_preventa.php');
            }else{
                alert("error");
                // alert(e);
            }
        }
    });
}
var eliminarArt = function(id_articulo){
    $("#ModalAutenticación").modal("show");
    $("#PassCliente").val('');
    $("#status").html("");
    $("#ejecutar").click(function(){
        var pass=$("#PassCliente").val();
        var user=$("#NamUsuario").val();
        $.ajax({
            type:"post",
            url:"view/js/admin.php?action=ExtraerPass",
            dataType:"json",
            data:{
                user:user,
                pass:pass
            }
        }).done(function(t){
            if (t.tipo_usuario=="admin") {
                if (t.equalPass=="si") {
                    $.ajax({
                    method: "post",
                    url: "controller/articulos.php?action=eliminar",
                    data: {id_articulo: id_articulo},
                    success: function(r){
                        if(r == 1){
                            alert("eliminado");
                            listar_articulos();
                        }else{
                            alert("Error al eliminar");
                            // alert(e);
                            }
                        }
                    });
                    $("#ModalAutenticación").modal("hide");
                     $("#status").html("CORRECTO");
                     $("#PassCliente").val('');
                }else{
                    $("#status").html("Contraseña incorrecta");   }//fin else pass
            }else{
                $("#status").html("Usted no tiene los permisos de administrador...");
            }
        });
    });//fin click en boton  


    //---------------------------------------------- 
}
//------------eliminar cliente ---------------//
var eliminarClientes = function(id_cliente){
    $("#ModalAutenticación").modal("show");
        $("#PassCliente").val();
    $("#ejecutar").click(function(){
        var pass=$("#PassCliente").val();
        var user=$("#NamUsuario").val();
        $.ajax({
            type:"post",
            url:"view/js/admin.php?action=ExtraerPass",
            dataType:"json",
            data:{
                user:user,
                pass:pass
            }
        }).done(function(t){
            if (t.tipo_usuario=="admin") {
                if (t.equalPass=="si") {
                    $.ajax({
                        method: "post",
                        url: "controller/clientes.php?action=eliminar",
                        data: {id_cliente: id_cliente},
                        success: function(r){
                            if(r == 1){
                                alert("CLIENTE ELIMINADO ");
                                listar_clientes();
                                $("#ModalAutenticación").modal("hide");
                            }else{
                                alert("ERROR EL CLIENTE NO SE PUEDE ELIMINAR PORQUE TIENE UNA DEUDA PENDIENTE...");
                                // alert(e);
                                }
                            }
                    });
                    $("#ModalAutenticación").modal("hide");
                     $("#status").html("CORRECTO");
                }else{
                    $("#status").html("Contraseña incorrecta");   }//fin else pass
            }else{
                $("#status").html("Usted no tiene los permisos de administrador...");
            }
        });
    });//fin click en boton   


}//fin funcion eliminarCliente
//------------eliminare Marcas ---------------//
var eliminarMarcas = function(id_marca){
    var msj=confirm("¿EN VERDAD DESEA ELIMINAR ESTA MARCA?");

    if (msj) {
        $.ajax({
        method: "post",
        url: "controller/marcas.php?action=eliminar",
        data: {id_marca: id_marca},
        success: function(r){
            if(r == 1){
                alert("eliminado");
                listar_marcas();
            }else{
                alert("error");
                // alert(e);
            }
        }
    });
    }
}
$('#editproveedor').click(function(){
        var datos = $('#form').serialize();
        $.ajax({
            type: "post",
            url:"controller/proveedor.php?action=editar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Actualizado con exito");
                }else{
                    alert("Hubo un error");
                }
            }
        });
        return false;
    });
$('#editarticulo').click(function(){
        //alert($("#codigo_articulo").val());
        var datos = $('#form5').serialize();
        $.ajax({
            type: "post",
            url:"controller/articulos.php?action=editar",
            data:datos,
            success: function(){
                 alert("Actualizado con exito");
            },error:function(){
                alert("Hubo un error al intentar modificar la información");
            }
        });
        return false;
    }); 
//------------Editar cliente ----------------//
$('#editCliente').click(function(){
        var datos = $('#form').serialize();
        $.ajax({
            type: "post",
            url:"controller/clientes.php?action=editar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Actualizado con exito");
                    
                }else{
                    alert("Hubo un error");
                }
            }
        });
        return false;
    });

//------------Editar marcas-----------------//
$('#editMarca').click(function(){
        var datos = $('#form').serialize();
        $.ajax({
            type: "post",
            url:"controller/marcas.php?action=editar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Actualizado con exito");
                    
                }else{
                    alert("Hubo un errorcillo");
                }
            }
        });
        return false;
    });
    
function proveedor(){
    var nombre_proveedor, telefono_proveedor, direccion;

    nombre_proveedor = document.getElementById("nombre_proveedor").value;
    telefono_proveedor = document.getElementById("telefono_proveedor").value;
    direccion = document.getElementById("direccion").value;

    if(nombre_proveedor === "" || telefono_proveedor === "" || direccion === ""){
        alert("Todos los campos son obligatorios");
        return false;
    }
    else if(nombre_proveedor.length>30) {
        alert("El nombre es muy largo");
        return false;
    }
    else if(telefono_proveedor.length>20) {
        alert("El telefono es largo");
        return false;
    }
    else if (direccion.length>45) {
        alert("la direccion es larga");
        return false;

    }else{

        var datos = $('#form').serialize();
        
        $.ajax({
            type: "post",
            url:"controller/proveedor.php?action=guardar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Agregado con exito");
                    listar_proveedor();
                }else{
                    alert("Hubo un error");
                    alert(e);
                }
            }
        });
        return false;
    }
}
// end save provedor
function articulo(){
    var codigo_articulo, clave_articulo, descripcion_articulo, unidad, codigo_barra, precio_c, precio_p, codigo_sat, descripcion_sat, marca, cantidad;

    codigo_articulo = document.getElementById("codigo_articulo").value;
    clave_articulo = document.getElementById("clave_articulo").value;
    descripcion_articulo = document.getElementById("descripcion_articulo").value;
    unidad = document.getElementById("unidad").value;
    codigo_barra = document.getElementById("codigo_barra").value;
    precio_c = document.getElementById("precio_c").value;
    precio_p = document.getElementById("precio_p").value;
    codigo_sat = document.getElementById("codigo_sat").value;
    descripcion_sat = document.getElementById("descripcion_sat").value;
    marca = document.getElementById("marca").value;
    cantidad = document.getElementById("cantidad").value;

    if( descripcion_articulo === ""){
        alert("Todos los campos son obligatorios");
        return false;

    }else{

        var datos = $('#form').serialize();
        
        $.ajax({
            type: "post",
            url:"controller/articulos.php?action=guardar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Agregado con exito");
                    listar_articulos();
                }else{
                    alert("Hubo un error");
                    alert(e);
                }
            }
        });
        return false;
    }
}
//-----------Agreagr y editar cliente---------//
function clientes(){
    var nombre_cliente, telefono_cliente, domicilio_cliente;

    nombre_cliente = document.getElementById("nombre_cliente").value;
    telefono_cliente = document.getElementById("telefono_cliente").value;
    domicilio_cliente = document.getElementById("domicilio_cliente").value;

    if(nombre_cliente === "" || telefono_cliente === "" || domicilio_cliente === ""){
        alert("Todos los campos son obligatorios");
        return false;
    }
    else if(nombre_cliente.length>30) {
        alert("El nombre es muy largo");
        return false;
    }
    else if(telefono_cliente.length>20) {
        alert("El numero es muy largo");
        return false;
    }
    else if (domicilio_cliente.length>50) {
        alert("la direccion es larga");
        return false;

    }else{

        var datos = $('#form1').serialize();
        
        $.ajax({
            type: "post",
            url:"controller/clientes.php?action=guardar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Agregado con exito");
                    listar_clientes();
                }else{
                    alert("Hubo un error");
                    alert(e);
                }
            }
        });
        return false;
    }
}
function marcas(){
    var nombre_marca;

    nombre_marca = document.getElementById("nombre_marca").value;


    if(nombre_marca === "" ){
        alert("Es necesario el nombre de la marca");
        return false;
    }else{

        var datos = $('#form1').serialize();
        
        $.ajax({
            type: "post",
            url:"controller/marcas.php?action=guardar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Agregado con exito");
                    listar_marcas();
                }else{
                    alert("Hubo un error");
                    alert(e);
                }
            }
        });
        return false;
    }
}

function ventas(){
    var venta;

    total_venta = document.getElementById("total_venta").value;


    if(total_venta === "" ){
        alert("Es necesario rellenar este campo");
        return false;
    }else{

        var datos = $('#id01').serialize();
        
        $.ajax({
            type: "post",
            url:"controller/ventas.php?action=guardar",
            data:datos,
            success: function(r){
                if(r == 1){
                    alert("Agregado con exito");
                    
                }else{
                    alert("Hubo un error");
                    alert(e);
                }
            }
        });
        return false;
    }
}




function verHistorial(id_cliente){
    $("#modalHistorial").modal("show");
    $("#idCliente").html(id_cliente);
    $.ajax({
          url:'view/js/admin.php?action=mostrarInfocliente',
          type:'POST',
          dataType:'json',
          data:{id_cliente:id_cliente}
          }).done(function(r){
            $("#tabla_historial tbody").html("");
            //alert(r.nombre_cliente+r.domicilio_cliente+r.telefono_cliente+r.credito);
            $("#nombre").html(r.nombre_cliente);
            $("#domicilio").html("Domicilio: "+ r.domicilio_cliente+" Col: "+r.colonia_cliente);
            $("#telefono").html("Telefono: "+r.telefono_cliente);
            $("#credito").html("Crédito actual: $ " + r.credito+" MXN");
             //alert(r.pru);
            for (var x=0;x<r.id_venta.length; x++) {     
            var cantidad=r.cantidad[x];
$("#tabla_historial tbody").append("<tr> <td>Nuevo</td> <td>"+r.id_venta[x]+"</td> <td>"+r.fecha[x]+"</td> <td>"+r.fecha_limite[x]+"</td> <td style='font-size: 70%'>"+cantidad+"</td> <td style='font-size: 70%'>"+r.producto[x]+"</td> <td style='font-size: 70%'>"+r.precio_pro[x]+"</td> <td style='font-size: 70%'>"+r.monto_p[x]+"<br>"+"<p style='color:#5676af;float:left'>Total=<br>$ "+r.importe[x]+"</p>"+"</td> <td>"+r.pagoInicial[x]+"<br><br>Nuevo= $"+r.comcredito[x]+"</td> <td><p style='color:red;font-size:100%'>$ "+r.saldo[x]+"</p></td> </tr>");
            }
            for (var y=0;y<r.fechaAbono.length;y++){
$("#tabla_historial tbody").append("<tr> <td>Abono</td> <td>  </td> <td>"+r.fechaAbono[y]+"</td>   <td></td>  <td></td> <td></td> <td></td> <td>"+r.cantidadAbono[y]+"</td> <td>"+r.credito_anterior[y]+"</td>    <td>$"+r.nuevo_credito[y]+"</td> </tr>");
                //alert(r.fechaAbono);
            }
            
            });//fin done 

}

