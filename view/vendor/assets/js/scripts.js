$(document).ready(function(){
    $('.menu-bar').on('click', function(){
        $('.contenido').toggleClass('abrir');
    });
    
    $('.sidebar-menu li:has(ul)').click(function(e){
        e.preventDefault();
        
        if ($(this).hasClass('activado')){
            $(this).removeClass('activado');
            $(this).children('ul').slideUp();
        } else{
            $('.sidebar-menu li ul').slideUp();
            $('.sidebar-menu li').removeClass('activado');
            $(this).addClass('activado');
            $(this).children('ul').slideDown();
        }
    });
});