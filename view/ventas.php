

<!-- tabla venta -->
  <div class="container-flui" >
      <div class="row">
          <div class="col">
              <div class="box box-blue">
                  <div class="box-body" style="padding: 0px">
                      <div class="table_responsive">
                        <div id="btn_header" style="width: 100%;">
                         <button class="btn btn-success d-none" id="ocultar" style="border-radius:5px 0 0 5px;height:20px;padding: 1px">
                            <i  class="zmdi  zmdi-eye" style="color:white;"></i>
                            %
                          </button>   
                        </div>
                           
                        <table id="tabla_venta" editabled="" class="table table-bordered display">
                             <thead>
                                 <tr>
                                  <th class='d-none'>Id</th>
                                  <th>cantidad</th>
                                  <td class="">Unidad</td>
                                  <th style="width: 100%">Descripción</th>
                                  <th class="d-none">%</th>
                                  <th class="headT">Precio</th>
                                  <th class="d-none">Exist</th>
                                  <th>Importe</th>
                                  <th><i class="zmdi zmdi-settings zmdi-lg"></i></th>
                                  <!-- <th><i class='fa fa-trash-o'></button></th> -->
                                 </tr>
                             </thead>
                             <tbody id="tbody_venta">
                             </tbody>
                         </table>     
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-body">
        <div class="table_responsive">
          <table id="tabla_busqueda" editabled="" class="table table-bordered" >
               <thead>
                   <tr>
                      <th>Id</th>
                      <th>Clave</th>
                      <th class="">Unidad</th>
                      <th style="width: 100%;">Descripción</th>
                      <th text-align="right">$ Precio</th>
                      <th text-align="right">Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
        
               </tbody>
           </table>   
        </div>
        </div>
      </div>
    </div>
  </div>

<!-- fin modal busqueda -->


<!-- $$$$$$$$$$$$$$$modal cobrar###################### -->
  <div class="modal fade" id="modal_cobrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-shopping-cart-plus" style="color:#FF336F"></i> Realizar venta</h3>
        </div>
        <div class="modal-body">

                      <h3 for="modal_total" class="Total" >Total: $</h3>
                      <h1 id="modal_total" name="modal_total" class="Total" style="color:#FF336F"></h1><br>

                      <label for="importe_modal">Importe en efectivo:</label>
                      <input type="number" id="importe_modal" autofocus class="form-control" name="importe_modal">

                      <h3 for="cambio" class="cambio">Cambio: $</h3>
                      <h1 id="cambio" name="cambio" class="cambio" style="color:#1AC144"></h1>
                      <br><br>
                      <button class=" btn btn-danger" id="realizarVenta"><i class="zmdi zmdi-shopping-cart-plus"></i>Aceptar</button>

                      <button class="btn btn-success" id="btn-cotizacion" style="float: right;margin-left: 15px;"> <i class="zmdi zmdi-card"></i>Cotización</button>
                      <button class="btn btn-primary" id="btn-credito"> <i class="zmdi zmdi-card"></i> A crédito</button><br><br>

                      <input type="text" class="DatosComprador form-control" id="NombreComprador" placeholder="Nombre cliente">
                      <input type="text" class="DatosComprador form-control" id="DirreccionComprador" placeholder="Dirección del cliente" style="margin-left:5px">
        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->

<!-- $$$$$$$$$$$$$$$modal cotizar###################### -->
  <div class="modal fade" id="modal_cotizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737;color: white"><h3><i class="zmdi zmdi-shopping-cart-plus" style="color:#FF336F"></i> Cotización</h3>
        </div>
        <div class="modal-body">
          <!-- <button class="btn btn-primary" id="print-ticket-cotizacion">imprimir en ticket</button> -->
          <button class="btn btn-success" id="imprimir_pdf_cotizacion">Imprimir pdf</button>
          
          <div id="print_pdf" class="content">
            <br><br>
             <div id="headerPrint" style="width: 100%;display: none" >
              <img src="view/img/headerSorian.png" style="width: 100%">
            </div>
            <label style="font-size: 120%">Nombre del cliente:<span><b id="Nombre_clienteContizacion"></b> </span></label><br>
            <label style="font-size: 120%">Dirección:<span><b id="direccion_clienteContizacion"></b> </span></label>
            
            <table class="table table-bordered display">
              <thead>
                <tr>
                  <th>Cantidad</th>
                  <th>Unidad</th>
                  <th>descripción</th>
                  <th>Costo unitario</th>
                  <th>Importe</th>
                </tr>
              </thead>
              <tbody id="tbody_cotizacion_pdf"></tbody>
            </table>
            <label style="float: right; font-size: 200%" class="text-danger" >Total= $<span id="importe_total"></span></label>
            <div id="pieCotizacion" style="width: 100%;display: none">
              <img src="view/img/pieCotizacion.jpg" style="width: 100%" >
            </div>
          </div>





        </div>
      </div>
    </div>
  </div>
<!-- fin modal cotizar -->



<!-- $$$$$$$$$$$$$$$$$$$$$$$$$$modal credito $$$$$$$$$$$$$$$$$$$$$-->
  <div class="modal fade" id="modal_credio" tabindex="1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737">
          <h3 id="titulo" style="color:#FEB625">Venta a crédito</h3>
        </div>
        <div id="navVC">
          <button class="btn btn-primary btnNav" id="liCredito" >Ventas a créditos</button>
        <button class="btn btn-success btnNav"  id="liNewCliente">Nuevo cliente</button>
      </div>

        <div class="modal-body">

          <div  id="div1">
            <h5 for="modal_total" class="Total">Total: $</h5>
                      <h4 id="modal_total_credito" name="modal_total" class="Total"></h4><br>
                      <label for="clientes">Cliente:</label>
                      <select name="clientes" class="form-control btn-primary" id="clientesPrestamo"  >
                        <option disabled="true" selected="">Selecciones un cliente</option>
                        
                      </select><br>
                      <div id="alerta">
                        
                      </div>
                      
                      <label for="fecha_limite">Fecha limite:</label>
                      <input type="date" required="true" name="fecha_limite" id="fecha_limite" class="form-control">
                      <label for ="pagoInicial">Pago inicial:</label>
                      <input type="number" name="pagoInicial" id="pagoInicial" value="0" class="form-control">
                      <br>
                      <br><br>
                      <input type="submit"  class=" btn btn-danger" id="realizarCredito" value="Realizar">
                      <br>
                      <div class="alert alert-success" role="alert">
                        Escriba manualmente el <span class="text-danger">número de ticket</span> #12345, en mov. del cliente. 
                      </div>
          </div>

          <div id="div2">
            <form id="form1" method="post" onsubmit="return clientes();">        
              <div class="form-group">
                  <label for="nombre_cliente" class="form-control-label">Nombre:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
                      <input type="text" name="nombre_cliente" class="form-control" id="nombre_cliente" autocomplete="off">
                  </div>
              </div>
               <div class="form-group">
                  <label for="domicilio_cliente" class="form-control-label">Domicilio:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
                      <input type="text" name="domicilio_cliente" class="form-control" id="domicilio_cliente" autocomplete="off">
                   </div>
              </div> 
              <div class="form-group">
                  <label for="colonia_cliente" class="form-control-label">Colonia o comunidad:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
                      <input type="text" name="colonia_cliente" class="form-control" id="colonia_cliente" autocomplete="off">
                   </div>
                </div>
              <div class="form-group">
                  <label for="municipio_cliente" class="form-control-label">Municipio y distrito:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
                      <input type="text" name="municipio_cliente" class="form-control" id="municipio_cliente"autocomplete="off">
                   </div>
              </div>
              <div class="form-group">
                  <label for="cp_cliente" class="form-control-label">Código postal:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-flag fa-lg"></i></div>
                      <input type="number" name="cp_cliente" class="form-control" id="cp_cliente" autocomplete="off">
                   </div>
              </div>
              <div class="form-group">
                  <label for="telefono_cliente" class="form-control-label">Telefono:</label>
                  <div class="input-group mb-2 mb-sm-0">
                      <div class="input-group-addon"><i class="fa fa-phone fa-lg"></i></div>
                      <input type="number" name="telefono_cliente" class="form-control" id="telefono_cliente" autocomplete="off">
                   </div>
              </div>
                           
              <div class="modal-footer">
                  <button type="submit" name="enviar" class="btn btn-primary" >GUARDAR</button>
                  <button type="reset" class="btn btn-warning">BORRAR</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>
<!-- fin modal a credito -->


<!-- ############estilo css############## -->
 

<!--***********************MODAL DEVOLUCIONES ***************************** -->
  <div class="modal fade" id="modal_devoluciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737">
          <h3 id="titulo" style="color:#FEB625">Devoluciones</h3>
        </div>
        <div class="modal-body">
          <label for="id_venta">Id venta:</label>
          <div>
            <input style="display: inline-block;width: 88%" type="number" name="id_venta" id="id_venta" class="form-control">
            <button style="display: inline-block;width: 9%;font-size: 30px;padding: 1px" class="btn btn-info" id="buscarVenta"><i class="zmdi zmdi-search"></i></button>
          </div>
            

            <p>Los productos que se pueden devolver son:</p>
              <div class="table_responsive">
                <table id="tabla_dev" editabled="" class="table table-bordered display">
                     <thead>
                         <tr>
                          <td>Id</td>
                            <th>cantidad</th>
                              <th>Descripción</th>
                              <th>$ Precio</th>
                              <th>$ Importe</th>
                         </tr>
                     </thead>
                     <tbody id="tbody_dev">
                     </tbody>
                 </table>     
              </div>

            <h3 for="modal_total" class="Total" id="txtTotal">Total a devolver: $</h3>
            <h1 id="modal_total_dev" name="modal_total" class="Total"></h1><br>

            <button id="ok_dev" class="btn btn-primary">Aceptar</button>


        </div>
      </div>
    </div>
  </div>
<!-- fin modal devoluciones -->



<!-- ####################moda corte################# -->
  <div class="modal fade" id="modal_corte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737">
          <h3 id="titulo" style="color:#FEB625">Corte z</h3>
        </div>
        <div class="modal-body">
          <h5>Información del sistema</h5>
          <label>Selecciona fecha:</label><br>
          <label for="inicial">De:</label>
          <input type="date" name="" id="fecha_inicial" style="color:#1AC139">
          <label>A:</label>
          <input type="date" name="" id="fecha_final" style="color:#1AC139">
          <button class="btn btn-info" id="BuscarInfo"><i class="zmdi zmdi-search"></i> Buscar</button>

          <div id="div_imprimir_corteZ">
            <label class="text-secondary" style="font-size: 150%" id="MostrarFechaenImpresion"></label> 

          <br>
          <div class="columna">
            <label for="N_ventas">Numero de ventas al contado:</label>
            <!-- <input type="number" id="N_ventas" disabled="true" class="form-control"> -->
            <span id="N_ventas" name="N_ventas" class="text-primary"></span><br>
            <label for="totalesVenta">Totales de venta al contado $:</label>
            <!-- <input type="text" name="" id="totalesVenta" disabled="true" class="form-control"> -->
            <span id="totalesVenta" name="totalesVenta" class="text-primary"></span><br>

            <label for="N_ventasCredito">Numero de ventas a crédito:</label>
            <!-- <input type="number" id="N_ventasCredito" disabled="true" class="form-control"> -->
            <span id="N_ventasCredito" name="N_ventasCredito" class="text-primary"></span><br>
            <label for="totalesVentaCredito">Totales de venta a crédito $:</label>
            <!-- <input type="text" name="" id="totalesVentaCredito" disabled="true" class="form-control"> -->
            <span id="totalesVentaCredito" name="totalesVentaCredito" class="text-primary"></span><br>
            <label for="totalesVentaCredito">Pago inicial de los créditos $:</label>
            <!-- <input type="text" name="" id="totalesVentaCredito" disabled="true" class="form-control"> -->
            <span id="pagoinicialCredito" name="pagoinicialCredito" class="text-primary"></span><br>

            <label for="totalesVentaCredito">Cantidad de turnos:</label>
            <span id="turnos" name="turnos" class="text-primary"></span><br>
            <label for="totalesVentaCredito">Total de inicio de turnos$:</label>
            <span id="totalturnos" name="totalturnos" class="text-primary"></span><br>
          </div>


          <div class="columna">
            <label>Cantidad de abonos:</label>
          <!-- <input type="text" name="" id="abonos" disabled="true" class="form-control"> -->
          <span id="abonos" name="abonos" class="text-primary"></span><br>
          <label>Totales de abonos $:</label>
          <!-- <input type="text" name="" id="totalAbono" disabled="true" class="form-control"> -->
          <span id="totalAbono" name="totalAbono" class="text-primary"></span><br>
          <label>Totales de salidas $:</label>
          <!-- <input type="text" name="" id="totalSalida" disabled="true" class="form-control"> -->
          <span id="totalSalida" name="totalSalida" class="text-primary"></span><br>


          <label>Cantidad de productos vendidos:</label>
          <!-- <input type="text" name="" id="productosVendidos" disabled="true" class="form-control"> -->
          <span id="productosVendidos" name="productosVendidos" class="text-primary"></span><br>
          <label>Totales de productos en existencia:</label>
          <!-- <input type="text" name="" id="productoExistencia" disabled="true" class="form-control"> -->
          <span id="productoExistencia" name="productoExistencia" class="text-primary"></span><br>
           <label for="total_en_caja">Total en caja :</label>
            <!-- <input type="text" name="" id="total_en_caja" disabled="true" class="form-control"> -->
            <span id="total_en_caja" name="total_en_caja" class="text-success"></span><br>

          </div>
          <br><br>
          </div>
         
          <button class="btn btn-danger" id="imprimirInforme"><i class="zmdi zmdi-print"></i>Imprimir corte</button>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
    .columna span{font-size: 150%}
  </style>
<!-- fin modal corte########## -->

<!-- ####################mMODAL INICIO DE TURNO ################# -->
  <div class="modal fade" id="iniciar_turno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header" style="background:#193737">
          <h3 id="titulo" style="color:#FEB625">Turno</h3>
        </div>
        <div class="modal-body">
          <input type="button" class="btn btn-primary" id="abrir" value="Abrir">
          <input type="button" class="btn btn-primary" id="cerrar" value="Cerrar">
          <input type="button" class="btn btn-primary" id="btn_mostrar_Rg_salida" value="Registrar Salida">
          <input type="button" class="btn btn-info" id="ver_salidas" value="Ver salidas">

          <div id="iniciar">
        
              <div class="form-group">
              <label for="en_caja" class="form-control-label">Efectivo en caja al inicio de turno:</label>
              <div class="input-group mb-2 mb-sm-0">
                  <div class="input-group-addon"><i class="zmdi zmdi-money-off">MNM</i></div>
                  <input type="number" name="en_caja" class="form-control" id="en_caja" value="0">
               </div>
              </div>
              <label for="comentario">Comentario</label>
              <textarea width="80%" name="comentario" id="comentario" class="form-control"></textarea><br>
              <p id="usuario" name="usuario"> <?php echo $_SESSION["usuario"] ?></p> 
              <button id="ok_turno"  class="btn btn-primary">Aceptar</button>     
          </div>
          <!-- ##################################################################################################################################################################################################### -->
          <div id="cerrarT"><!--div cerrar T -->
            <div class="row">
              <div class="col-xl-6 col-md-6 mb-4">
                <div class="form-group">
            <label for="en_caja" class="form-control-label">Total de ventas: $</label>
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><i class="zmdi zmdi-money-off">MNM</i></div>
                <input type="number" name="total_venta" disabled="true" class="form-control" id="total_venta" value="0">
              </div>
           </div>
           <div class="form-group">
            <label for="en_caja" class="form-control-label">Total de salidas: $</label>
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><i class="zmdi zmdi-money-off">MNM</i></div>
                <input type="number" name="total_salida" disabled="true" class="form-control" id="total_salida" value="0">
              </div>
           </div>
            <div class="form-group">
              <!-- Total en caja por el sistema -->
              <input type="number" name="en_caja_fin_system"  class="form-control d-none" id="en_caja_fin_system" value="0" readonly="" >

            <label for="en_caja" class="form-control-label">Total en caja: $</label>
            <!-- Total en caja por el usuario -->
            <div class="input-group mb-2 mb-sm-0">
                <div class="input-group-addon"><i class="zmdi zmdi-money-off">MNM</i></div>
                <input type="number" name="en_caja_fin"  class="form-control" id="en_caja_fin" value="0" >
              </div>

              
           </div>

              </div>
              <div class="col-xl-6 col-md-6 mb-4">
                <label>Diferencia</label>
              <input type="number" id="diferencia" name="diferencia" class="form-control" value="0.00" readonly="" >
                <label for="comentario">Comentario</label>
                <textarea width="80%" name="comentario_fin" id="comentario_fin" class="form-control"></textarea><br>
              </div>
            </div>
            <button id="ok_turno_cierre"  class="btn btn-primary float-right  ">Cerrar turno <i class="fa fa-power-off"></i></button> 
          </div>
          <!-- ##################################################### -->
          <!-- $##################################################### -->
          <br><br>
          <span class="text-primary">Usuario:</span><span id="usuario" name="usuario" class="text-success"> <?php echo $_SESSION["usuario"] ?> </span>

          <div id="Registrar_Salidas">
            <h4>Registrar salidas</h4>
            <label>Concepto:</label>
                <input type="text" id="concepto" class="form-control">
                <label>Cantidad: $</label>
                <input type="number" id="cantidad" class="form-control" placeholder="$"> <br>
                <button id="btn_reg_salida" class="btn btn-primary">Registrar</button>
          </div>
          <!-- -----------ver salidas-------------- --><br><br>
          <div id="tab_salidas">
           <div class="" id="seccion_print">
            <span>turno: </span><span id="turno_salida"></span><br>
            <span>Vendedor: </span><span id="usuario_salida"></span><br>
            <span>Fecha y hora: </span><span id="fecha_salida"></span><br>
            <table class="table table-bordered display">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Concepto</th>
                  <th>Cantidad $</th>
                  <th>Fecha</th>
                </tr>
              </thead>
              <tbody id="tab_ver_salidas">
                
              </tbody>
            </table>
            <br>
            
           </div>
           <button class="btn btn-danger float-left" id="btn_print_salidas_ticket">Imprimir en ticket</button>
            <button class="btn btn-success float-right" id="imprimir_salida">Imprimir</button>
         </div>
            
          <!-- fin ver saldas -->
                                 
        </div>
      </div>
    </div>
  </div>
<!-- fin modal corte########## -->

